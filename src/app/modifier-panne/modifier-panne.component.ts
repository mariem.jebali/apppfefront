import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import Swal from "sweetalert2";
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { panneService } from '../services/panneService';
@Component({
  selector: 'app-modifier-panne',
  templateUrl: './modifier-panne.component.html',
  styleUrls: ['./modifier-panne.component.css']
})
export class ModifierPanneComponent implements OnInit {
  form: FormGroup
 panne
  sub
  id
  ArrayPanne
    errorMessage
  constructor( private panneService:panneService, private router:Router, private ActivatedRoute:ActivatedRoute){
    this.form = new FormGroup({
      libelle_panne: new FormControl(null, Validators.required),
    })
  }
  ngOnInit() {
    this.getOnePanne();
  }
  isValid(controlName) {
    return this.form.get(controlName).invalid && this.form.get(controlName).touched;}

  getOnePanne(){
    this.sub= this.ActivatedRoute.params.subscribe(params => {
      this.id= params['id'];
      console.log(this.id);
      this.panneService.getOnePanne(this.id)
          .subscribe( data =>{
                console.log(data);
                this.panne = data;
                 this.ArrayPanne=Array(this.panne)
                  this.form.get('libelle_panne').setValue(this.panne.libelle_panne)
              },
              error => {
                console.log(error);
              }
          )

    })
  }
  modifierPanne(){

    console.log(this.form.value);
    if (this.form.valid) {
        this.sub= this.ActivatedRoute.params.subscribe(params => {
            this.id= params['id'];
        this.panneService.modifierPanne(this.form.value, this.id)
            .subscribe(
                data => {
                    console.log(data);
                    this.router.navigate(['../pannes']);
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Panne modifié avec succès',
                        showConfirmButton: false,
                        timer: 1500
                    })
                },
                error => {
                    console.log(error.error['message']);
                    this.errorMessage = error.error['message']

                }
            );
        })
    }
      }

  reset(){
    this.router.navigate(['../pannes']);
  }

}
