import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifierPanneComponent } from './modifier-panne.component';

describe('ModifierPanneComponent', () => {
  let component: ModifierPanneComponent;
  let fixture: ComponentFixture<ModifierPanneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifierPanneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifierPanneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
