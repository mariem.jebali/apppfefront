import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule , Routes} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';
import {MatFormFieldModule} from '@angular/material/form-field';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import {AuthService } from './services/authService';
import { fonctionsService } from './services/fonctionsService';
import { utilisateurService } from './services/utilisateurService';
import { statistiqueService} from './services/statistique';
import { pieceService } from './services/pieceService';
import { panneService } from './services/panneService';
import { reclamationService } from './services/reclamationService';
import { emplacementService } from './services/emplacementService';
import { ordreTravailService } from  './services/ordreTravailService';
import { intervenantService } from  './services/intervenantService';
import { ToastrModule } from 'ngx-toastr';
import {MatSelectModule} from '@angular/material/select';
import { NgSelectModule } from '@ng-select/ng-select';


@NgModule({
    imports: [
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        ComponentsModule,
        RouterModule,
        AppRoutingModule,
        HttpClientModule,
        ToastrModule.forRoot(),
        MatFormFieldModule,
        MatSelectModule,
        NgSelectModule,
    ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    LoginComponent,

  ],
  providers: [AuthService, fonctionsService, utilisateurService, pieceService, panneService,
      reclamationService, emplacementService,ordreTravailService,intervenantService,statistiqueService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
