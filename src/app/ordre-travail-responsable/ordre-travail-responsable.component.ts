import { Component, OnInit } from '@angular/core';
import {ordreTravailService} from '../services/ordreTravailService';
import {Router} from '@angular/router';
@Component({
  selector: 'app-ordre-travail-responsable',
  templateUrl: './ordre-travail-responsable.component.html',
  styleUrls: ['./ordre-travail-responsable.component.css']
})
export class OrdreTravailResponsableComponent implements OnInit {
  ordres
  href: string = ""
  isNonTraiter: boolean;
  isTraiter: boolean;
  isCloturer: boolean;
  array
    total
  constructor(private ordreService : ordreTravailService,private   router: Router){}
  ngOnInit() {
    this.getOrdretravail();

  }
  getOrdretravail(){

    this.href = this.router.url;
    if (this.router.url.startsWith('/ordreTravailNonTraitees'))
    {this.getRespOtNonTraiter();
      this.isNonTraiter=true
    }else if (this.router.url.startsWith ('/ordreTravailTraitees'))
    {this.getRespOtTraiter();
      this.isTraiter=true
    }else if (this.router.url.startsWith('/ordreTravailCloturees'))
    {this.getRespOtCloturer();
      this.isCloturer=true}

  }
  getRespOtTraiter(){
    this.ordreService.getRespOtTraiter()
        .subscribe( data =>{
              console.log(data)
              this.ordres= data;
            },
            error => {
              console.log(error)
            }
        )
  }

  getRespOtCloturer(){
    this.ordreService.getRespOtCloturer()
        .subscribe( data =>{
              console.log(data)
              this.ordres= data;
              this.array=Array(this.ordres)
            this.total=this.ordres.length
                console.log( this.total)

            },
            error => {
              console.log(error)
            }
        )
  }
  getRespOtNonTraiter(){
    this.ordreService.getRespOtNonTraiter()
        .subscribe( data =>{
              console.log(data)
              this.ordres= data;
            },
            error => {
              console.log(error)
            }
        )
  }


}
