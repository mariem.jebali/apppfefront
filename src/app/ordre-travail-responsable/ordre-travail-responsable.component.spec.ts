import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdreTravailResponsableComponent } from './ordre-travail-responsable.component';

describe('OrdreTravailResponsableComponent', () => {
  let component: OrdreTravailResponsableComponent;
  let fixture: ComponentFixture<OrdreTravailResponsableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdreTravailResponsableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdreTravailResponsableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
