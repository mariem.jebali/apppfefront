import { Component, OnInit } from '@angular/core';
import {panneService } from '../services/panneService';
import {emplacementService } from '../services/emplacementService';
import {reclamationService } from '../services/reclamationService';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { NgSelectModule, NgSelectConfig } from '@ng-select/ng-select';
import {Router} from '@angular/router';

import Swal from "sweetalert2";
@Component({
  selector: 'app-add-reclamation',
  templateUrl: './envoyer-reclamation.component.html',
  styleUrls: ['./envoyer-reclamation.component.css']
})
export class EnvoyerReclamationComponent implements OnInit {
    selectedpiece:[]
    panneId
    emplacementId
    form:FormGroup
  constructor( private panneService:panneService , private emplacementService: emplacementService,
               private  reclamationService:reclamationService, private router:Router) {
      this.form = new FormGroup({
          PanneId: new FormControl(null, Validators.required),
          priorite: new FormControl(null, Validators.required),
          empId: new FormControl(null, Validators.required),
          observation: new FormControl(null, Validators.required),
      })
  }
  ngOnInit(){
    this.getAllPanne();
    this.getAllEmplacement();
  }
    isValid(controlName) {
        return this.form.get(controlName).invalid && this.form.get(controlName).touched;
    }
  getAllPanne() {
    this.panneService.getAllPannes()
        .subscribe(
            data => {
              console.log(data)
              this.panneId = data
            },
            error => {
              console.log(error)
            })
  }
    getAllEmplacement() {
        this.emplacementService.getAllEmplacement()
            .subscribe(
                data => {
                    console.log(data)
                    this.emplacementId = data
                },
                error => {
                    console.log(error)
                })
    }

    addReclamation() {
        console.log(this.form.value);
        if (this.form.valid) {
            this.reclamationService.addReclamation(this.form.value)
                .subscribe(
                    data => {
                        console.log(data);
                        this.router.navigate(['../reclamationsEnvoyees']);
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Reclamation envoyé avec succès',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    },
                    error => {
                        console.log(error.error['message']);
                        // this.errorMessage = error.error['message']

                    }
                );

        }
    }
    reset(){
        this.router.navigate(['../home']);
    }


}
