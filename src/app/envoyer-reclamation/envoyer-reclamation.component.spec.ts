import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnvoyerReclamationComponent } from './envoyer-reclamation.component';

describe('AddReclamationComponent', () => {
  let component: EnvoyerReclamationComponent;
  let fixture: ComponentFixture<EnvoyerReclamationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnvoyerReclamationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnvoyerReclamationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
