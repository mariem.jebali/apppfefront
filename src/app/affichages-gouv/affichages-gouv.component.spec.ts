import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffichagesGouvComponent } from './affichages-gouv.component';

describe('AffichagesComponent', () => {
  let component: AffichagesGouvComponent;
  let fixture: ComponentFixture<AffichagesGouvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffichagesGouvComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffichagesGouvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
