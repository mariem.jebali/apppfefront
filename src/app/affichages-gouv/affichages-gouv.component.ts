import { Component, OnInit } from '@angular/core';
import {Router , ActivatedRoute} from '@angular/router';
import { utilisateurService } from '../services/utilisateurService';
import{ reclamationService } from '../services/reclamationService';

@Component({
  selector: 'app-affichages',
  templateUrl: './affichages-gouv.component.html',
  styleUrls: ['./affichages-gouv.component.css']
})
export class AffichagesGouvComponent implements OnInit {
    reclamations
  href: string = ""
 id
    sub

    arrayReclamation
  constructor(private utilisateurService : utilisateurService , private reclamationService:reclamationService,
              private   router: Router, private ActivatedRoute:ActivatedRoute) { }

  ngOnInit() {
    this.getReclamationDetails()

  }

  // afficher(){
  //     this.AuthService.getDetailMe()
  //         .subscribe( data =>{
  //                 console.log(data)
  //                 this.detailUser = data;
  //                 this.href = this.router.url;
  //                 if (this.detailUser.fonctionId == 2) {
  //                     this.getReclamationDetails()
  //                     this.isGouv = true
  //                 }else if (this.detailUser.fonctionId == 3){
  //                     this.getPlanningInterv()
  //                     this.isResp=true
  //                 }
  //             },
  //             error => {
  //                 console.log(error)
  //             }
  //         )
  // }

getReclamationDetails(){
    this.sub= this.ActivatedRoute.params.subscribe(params => {
        this.id= params['id'];
        console.log(this.id);
        this.reclamationService.getReclamationdetail(this.id)
            .subscribe( data =>{
                    console.log(data);
                    this.reclamations = data;
                    this.arrayReclamation= Array(this.reclamations)

                },
                error => {
                    console.log(error)
                }
            )
    });
}

    backToRec(){

        this.router.navigate(['../reclamationsEnvoyees'])
    }


}
