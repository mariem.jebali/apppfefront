import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/authService';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
details
  constructor( private AuthService:AuthService ) { }

  ngOnInit(){
    this.getUserDetail();

  }


  getUserDetail(){
    this.AuthService.getDetailMe()
        .subscribe( data =>{
              console.log(data)
              this.details = data;
            },
            error => {
              console.log(error)
            }
        )
  }
}
