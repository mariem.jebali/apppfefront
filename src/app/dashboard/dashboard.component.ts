import {Component, OnInit} from '@angular/core';
import * as Chartist from 'chartist';
import {ordreTravailService} from '../services/ordreTravailService';
import {reclamationService} from '../services/reclamationService';
import {statistiqueService} from '../services/statistique';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
    recgouv
    pieces
    ordres
    reclamations
    totalCloturer
    totalTraiter
    totalNonTraiter
    totalenv
    reclamationsChart = []
    countgouv = []
    nomgouv = []
    prenomgouv = []
    respectnonrespect = []
    countpanne = []
    libellepanne = []
    countemplacement=[]
    typeemplacement=[]
    public lineChartData: Array<any>;
    public lineChartLabels: Array<any>;
    public lineChartOptions: any;
    public lineChartColors: Array<any>;
    public lineChartLegend: boolean;
    public lineChartType

    public lineChartData2: Array<any>;
    public lineChartLabels2: Array<any>;
    public lineChartOptions2: any;
    public lineChartColors2: Array<any>;
    public lineChartLegend2: boolean;
    public lineChartType2

    public lineChartData3: Array<any>;
    public lineChartLabels3: Array<any>;
    public lineChartOptions3: any;
    public lineChartColors3: Array<any>;
    public lineChartLegend3: boolean;
    public lineChartType3

    public lineChartData4: Array<any>;
    public lineChartLabels4: Array<any>;
    public lineChartOptions4: any;
    public lineChartColors4: Array<any>;
    public lineChartLegend4: boolean;
    public lineChartType4

    public lineChartData5: Array<any>;
    public lineChartLabels5: Array<any>;
    public lineChartOptions5: any;
    public lineChartColors5: Array<any>;
    public lineChartLegend5: boolean;
    public lineChartType5

    constructor(private ordreService: ordreTravailService,
                private reclamationService: reclamationService, private statistiqueService: statistiqueService) {
    }

    ngOnInit() {
        this.totalOTCloturer()
        this.totalOTNonTraiter()
        this.totalOTTraiter()
        this.totalRecEnvoyer()
        this.statRecJour();
        //this.statRecGouv();
        this.respectnonrespectdate();
        this.statPanne()
        this.stockepuise()
        this.recParGouv();
        this.statTypeEmplacement()
    }

    /*------------------------------------------ stat globale--------------------------------------------------------------  */

    //traiter
    totalOTTraiter() {
        this.ordreService.getRespOtTraiter()
            .subscribe(data => {
                    console.log(data)
                    this.ordres = data;
                    this.totalTraiter = this.ordres.length
                    console.log(this.totalTraiter)
                },
                error => {
                    console.log(error)
                }
            )
    }

//cloturer
    totalOTCloturer() {
        this.ordreService.getRespOtCloturer()
            .subscribe(data => {
                    console.log(data)
                    this.ordres = data;
                    this.totalCloturer = this.ordres.length
                    console.log(this.totalCloturer)

                },
                error => {
                    console.log(error)
                }
            )
    }

//non traiter
    totalOTNonTraiter() {
        this.ordreService.getRespOtNonTraiter()
            .subscribe(data => {
                    console.log(data)
                    this.ordres = data;
                    this.totalNonTraiter = this.ordres.length
                    console.log(this.totalNonTraiter)
                },
                error => {
                    console.log(error)
                }
            )
    }

//envoyer
    totalRecEnvoyer() {
        this.reclamationService.getRespReclamation()
            .subscribe(data => {
                    console.log(data)
                    this.reclamations = data;
                    this.totalenv = this.reclamations.length
                    console.log(this.totalenv)
                },
                error => {
                    console.log(error)
                }
            )
    }

    /*--------------------------------statistique: reclamation par jour-----------------------------------*/

    statRecJour() {
        this.statistiqueService.recEnvoyerJour()
            .subscribe(data => {
                    console.log(data)
                    // this.rec1= data;
                    this.reclamationsChart.push(data[0]['RecCount'])

                },
                error => {
                    console.log(error)
                }
            )
        this.statistiqueService.recNonTraiterJour()
            .subscribe(data => {
                console.log(data)
                this.reclamationsChart.push(data[0]['RecCount'])

            }, error => {
                console.log(error)
            })

        this.statistiqueService.recTraiterJour()
            .subscribe(data => {
                console.log(data)
                this.reclamationsChart.push(data[0]['RecCount'])
            }, error => {
                console.log(error)
            })
        this.lineChartData = [{data: this.reclamationsChart, fill: false}];
        this.lineChartLabels = ['Envoyées', 'Non Traitées', 'Traitées'];
        this.lineChartOptions = {
            responsiveAnimationDuration: 5000,
            animation: {},
            title: {
                display: true,
                // text: 'Relamation par jour',
                fontColor: '#12b0bc',
                fontSize: 18,
                fontStyle: 'normal',
                fontFamily: '\'Quicksand\''
            },
            scales: {
                xAxes: [{display: false}],
                yAxes: [{
                    display: false,
                    scaleLabel: {
                        display: true,
                        // labelString: 'Relamation par jour ( CM )'
                    },
                    ticks: {
                        beginAtZero: true,
                    }
                }]
            },

        };
        this.lineChartColors = [
            {
                backgroundColor: ['#87CEEB', '#FF7F50', '#2E8B57'],
                borderColor: '',
                pointBackgroundColor: '#000000',
                pointBorderColor: '#fff',
                pointHoverBackgroundColor: '#fff',
                pointHoverBorderColor: 'rgba(148,159,177,0.😎'
            }
        ];
        this.lineChartLegend = true;
        this.lineChartType = 'pie';
    }

    /*--------------------------------- stat reclamation par gouvernante---------------------------------------------------*/


    // statRecGouv() {
    //     this.statistiqueService.gouvPanne()
    //         .subscribe(data => {
    //                 console.log(data)
    //                 for (var i = 0; i < data['length']; i++) {
    //                     this.countgouv.push(data[i]['GouvCount'])
    //                     this.nomgouv.push(data[i]['Gouvernante']['nom'])
    //                 }
    //             },
    //             error => {
    //                 console.log(error)
    //             }
    //         )
    //     this.lineChartData2 = [{data: this.countgouv, fill: false}];
    //     this.lineChartLabels2 = this.nomgouv;
    //     this.lineChartOptions2 = {
    //         responsiveAnimationDuration: 5000,
    //         animation: {},
    //         title: {
    //             display: true,
    //             // text: 'Relamation par jour',
    //             fontColor: '#12b0bc',
    //             fontSize: 18,
    //             fontStyle: 'normal',
    //             fontFamily: '\'Quicksand\''
    //         },
    //         scales: {
    //             xAxes: [{
    //                 display: false
    //
    //             }],
    //             yAxes: [{
    //                 display: false,
    //                 scaleLabel: {
    //                     display: true,
    //                     //  labelString: 'Panne par gouvernante'
    //                 },
    //                 ticks: {
    //                     beginAtZero: true,
    //                 }
    //             }]
    //         },
    //
    //     };
    //     this.lineChartColors2 = [
    //         {
    //             backgroundColor: ['#E9967A','#B22222','#DEB887','#F4A460','#FFDEAD','#F0FFFF','#FFE4E1','#FFA500', '#53B6F8', '#CE2018', '#40C26B', '#C240B6', '#F19634', '#6A5ACD', '#FFB6C1', '#9400D3', '#D8BFD8', '#87CEEB'],
    //             borderColor: '',
    //             pointBackgroundColor: '#000000',
    //             pointBorderColor: '#fff',
    //             pointHoverBackgroundColor: '#fff',
    //             pointHoverBorderColor: 'rgba(148,159,177,0.😎'
    //         }
    //     ];
    //     this.lineChartLegend2 = true;
    //     this.lineChartType2 = 'pie';
    // }
    /*--------------------------------- statiqtique des pannes -------------------------------------------*/
    statPanne() {
        this.statistiqueService.statpanne()
            .subscribe(data => {
                    console.log(data)

                    for (var i = 0; i < data['length']; i++) {
                        this.countpanne.push(data[i]['panneCount'])
                        this.libellepanne.push(data[i]['libelle_panne'])
                    }

                },
                error => {
                    console.log(error)
                }
            )
        this.lineChartData4 = [{data: this.countpanne, fill: false}];
        this.lineChartLabels4 = this.libellepanne;
        this.lineChartOptions4 = {
            responsiveAnimationDuration: 5000,
            animation: {},
            title: {
                display: true,
                // text: 'Relamation par jour',
                fontColor: '#12b0bc',
                fontSize: 18,
                fontStyle: 'normal',
                fontFamily: '\'Quicksand\''
            },
            scales: {
                xAxes: [{display: false}],
                yAxes: [{
                    display: false,
                    scaleLabel: {
                        display: true,
                        // labelString: 'Relamation par jour ( CM )'
                    },
                    ticks: {
                        beginAtZero: true,
                    }
                }]
            },

        };
        this.lineChartColors4 = [
            {
                backgroundColor: ['#D8BFD8', '#FF69B4','#F0E68C','#DA70D6',
                    '#FFA500','#98FB98','#6A5ACD','#FFDAB9', '#FFB6C1',
                    '#9400D3', '#87CEEB','#53B6F8',
                    '#008080', '#6495ED', '#FF00FF','#FA8072','#B22222','#3CB371','#8B008B'],
                borderColor: '',
                pointBackgroundColor: '#000000',
                pointBorderColor: '#fff',
                pointHoverBackgroundColor: '#fff',
                pointHoverBorderColor: 'rgba(148,159,177,0.😎'
            }
        ];
        this.lineChartLegend4 = true;
        this.lineChartType4 = 'pie';
    }


    /*-------------------------------- le respect des délais de réparation des pannes----------------------------------------------*/
    respectnonrespectdate() {
        this.statistiqueService.respectDate()
            .subscribe(data => {
                    console.log(data)
                    this.respectnonrespect.push(data[0]['respectCount'])

                },
                error => {
                    console.log(error)
                }
            )
        this.statistiqueService.nonRespectDate()
            .subscribe(data => {
                console.log(data)
                this.respectnonrespect.push(data[0]['respectCount'])

            }, error => {
                console.log(error)
            })
        this.lineChartData3 = [{data: this.respectnonrespect, fill: false}];
        this.lineChartLabels3 = ['Délai respecté', 'Délai non respecté'];
        this.lineChartOptions3 = {
            responsiveAnimationDuration: 5000,
            animation: {},
            title: {
                display: true,
                // text: 'Relamation par jour',
                fontColor: '#12b0bc',
                fontSize: 18,
                fontStyle: 'normal',
                fontFamily: '\'Quicksand\''
            },
            scales: {
                xAxes: [{
                    display: false
                }],
                yAxes: [{
                    display: false,
                    scaleLabel: {
                        display: true,
                        // labelString: 'Relamation par jour ( CM )'
                    },
                    ticks: {
                        beginAtZero: true,
                    }
                }]
            },

        };
        this.lineChartColors3 = [
            {
                backgroundColor: ['#FF7F50', '#F5DEB3'],
                borderColor: '',
                pointBackgroundColor: '#000000',
                pointBorderColor: '#fff',
                pointHoverBackgroundColor: '#fff',
                pointHoverBorderColor: 'rgba(148,159,177,0.😎'
            }
        ];
        this.lineChartLegend3 = true;
        this.lineChartType3 = 'pie';
    }
/*----------------------- tableaux ------------------------------- */
//pièce à epuisée
    stockepuise() {
        this.statistiqueService.getStockEpuise()
            .subscribe(data => {
                    console.log(data)
                    this.pieces = data;
                },
                error => {
                    console.log(error)
                }
            )
    }
    // rec par gouv
   recParGouv() {
        this.statistiqueService.gouvPanne()
            .subscribe(data => {
                    console.log(data)
                    this.recgouv = data;
                },
                error => {
                    console.log(error)
                }
            )
    }
    /*---------------------- rec par type emplacement-------------------------------- */
    statTypeEmplacement() {
        this.statistiqueService.statTypeEmplacement()
            .subscribe(data => {
                    console.log(data)
                    for (var i = 0; i < data['length']; i++) {
                        this.countemplacement.push(data[i]['emplacementCount'])
                        this.typeemplacement.push(data[i]['type'])
                    }
                },
                error => {
                    console.log(error)
                }
            )
        this.lineChartData5 = [{data: this.countemplacement, fill: false}];
        this.lineChartLabels5 = this.typeemplacement;
        this.lineChartOptions5 = {
            responsiveAnimationDuration: 5000,
            animation: {},
            title: {
                display: true,
                // text: 'Relamation par jour',
                fontColor: '#12b0bc',
                fontSize: 18,
                fontStyle: 'normal',
                fontFamily: '\'Quicksand\''
            },
            scales: {
                xAxes: [{
                    display: false

                }],
                yAxes: [{
                    display: false,
                    scaleLabel: {
                        display: true,
                        //  labelString: 'Panne par gouvernante'
                    },
                    ticks: {
                        beginAtZero: true,
                    }
                }]
            },

        };
        this.lineChartColors5 = [
            {
                backgroundColor: ['#E9967A','#B22222','#DEB887','#F4A460','#FFDEAD','#F0FFFF','#FFE4E1','#FFA500', '#53B6F8', '#CE2018', '#40C26B', '#C240B6', '#F19634', '#6A5ACD', '#FFB6C1', '#9400D3', '#D8BFD8', '#87CEEB'],
                borderColor: '',
                pointBackgroundColor: '#000000',
                pointBorderColor: '#fff',
                pointHoverBackgroundColor: '#fff',
                pointHoverBorderColor: 'rgba(148,159,177,0.😎'
            }
        ];
        this.lineChartLegend5 = true;
        this.lineChartType5 = 'pie';
    }

}

