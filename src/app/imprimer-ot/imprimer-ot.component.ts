import { Component, OnInit } from '@angular/core';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
import {ordreTravailService} from '../services/ordreTravailService';
import {Router, ActivatedRoute} from '@angular/router';
@Component({
  selector: 'app-imprimer-ot',
  templateUrl: './imprimer-ot.component.html',
  styleUrls: ['./imprimer-ot.component.css']
})
export class ImprimerOTComponent implements OnInit {
ordres
  sub
  id
  constructor(private   router: Router, private ActivatedRoute: ActivatedRoute,
              private  ordreService: ordreTravailService,) { }

  ngOnInit() {
    this.getRespOtNonTraiter();
  }
  getRespOtNonTraiter() {
    this.sub = this.ActivatedRoute.params.subscribe(params => {
      this.id = params['id'];
      this.ordreService.getOtNonTraiterDetail(this.id)
          .subscribe(data => {
                console.log(data)
                this.ordres = data;
              },
              error => {
                console.log(error)
              }
          )
    })
  }
  imprimer(){
    var data = document.getElementById('convert');
    html2canvas(data).then(canvas => {
      // Few necessary setting options
      var imgWidth = 208;
      var pageHeight = 295;
      var imgHeight = canvas.height * imgWidth / canvas.width;
      var heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL('image/png')
      let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF
      var position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
      pdf.save('reclamation.pdf'); // Generated PDF
    });
  }

  backToNonTraiter() {
    this.router.navigate(['../ordreTravailNonTraitees'])}

  }
