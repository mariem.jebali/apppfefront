import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImprimerOTComponent } from './imprimer-ot.component';

describe('ImprimerOTComponent', () => {
  let component: ImprimerOTComponent;
  let fixture: ComponentFixture<ImprimerOTComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImprimerOTComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImprimerOTComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
