import { Component, OnInit } from '@angular/core';
import {FormGroup , FormControl, Validators} from '@angular/forms';
import { AuthService } from '../services/authService';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-upgrade',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
loginForm: FormGroup;
    errorMessage

  constructor( private authService: AuthService ,
            private   router: Router ,
             private  _activatedRouter: ActivatedRoute) {
    this.loginForm = new FormGroup({
      email: new FormControl(null, Validators.required),
      mdp : new FormControl(null, Validators.required)
    })
  }

  ngOnInit() {}
 isValid(controlName) {
    return this.loginForm.get(controlName).invalid && this.loginForm.get(controlName).touched;
 }
    login() {
    console.log(this.loginForm.value);
    if (this.loginForm.valid) {
        this.authService.login(this.loginForm.value)
        .subscribe(
            data => {
                console.log(data);
                localStorage.setItem('token', data.toString());
                localStorage.setItem( 'validtoken', 'true');
                this.router.navigate(['../home']);

            },
                error => {
                    console.log(error.error['message']);
                    this.errorMessage = error.error['message']

            }
        );
    }
    }
}
