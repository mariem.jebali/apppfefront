import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifierOTComponent } from './modifier-ot.component';

describe('ModifierOTComponent', () => {
  let component: ModifierOTComponent;
  let fixture: ComponentFixture<ModifierOTComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifierOTComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifierOTComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
