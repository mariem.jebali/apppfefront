import { Component, OnInit } from '@angular/core';
import Swal from "sweetalert2";

import {Router, ActivatedRoute} from '@angular/router';
import {ordreTravailService} from '../services/ordreTravailService';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {intervenantService} from '../services/intervenantService';
@Component({
  selector: 'app-modifier-ot',
  templateUrl: './modifier-ot.component.html',
  styleUrls: ['./modifier-ot.component.css']
})
export class ModifierOTComponent implements OnInit {
  form: FormGroup
  errorMessage
  UtilisateurId
  sub
  id
  ordre
  arrayOrdre
  constructor(private   router: Router,private intervenantService :intervenantService ,
              private ActivatedRoute:ActivatedRoute, private  ordreService: ordreTravailService){
    this.form = new FormGroup({
      date_deb_interv: new FormControl(null, Validators.required),
      date_limit_interv: new FormControl(null, Validators.required),
      UtilisateurId: new FormControl(null, Validators.required),
    })
  }

  ngOnInit(){
    this.getAllIntervenant();
    this.getOneOT()
  }
  isValid(controlName) {
    return this.form.get(controlName).invalid && this.form.get(controlName).touched;}

  getAllIntervenant(){
    this.intervenantService.getAllIntervenant()
        .subscribe(data => {
              console.log(data)
              this.UtilisateurId = data;
            },
            error => {
              console.log(error)
            }
        )
  }
  modifierOT(){
    console.log(this.form.value);
    if (this.form.valid) {
      this.sub= this.ActivatedRoute.params.subscribe(params => {
        this.id= params['id'];
      this.ordreService.modifierOT(this.form.value,this.id)
          .subscribe(
              data => {
                console.log(data);
                this.router.navigate(['../ordreTravailNonTraitees']);
                Swal.fire({
                  position: 'center',
                  icon: 'success',
                  title: 'Ordre de travail modifié avec succès',
                  showConfirmButton: false,
                  timer: 1500
                })

              },
              error => {
                console.log(error.error['message']);
                this.errorMessage = error.error['message']

              }
          );

      })

    }}
  getOneOT(){
    this.sub= this.ActivatedRoute.params.subscribe(params => {
      this.id= params['id'];
      this.ordreService.getOtNonTraiterDetail(this.id)
          .subscribe( data =>{
                console.log(data);
                this.ordre= data;
              this.arrayOrdre=Array(this.ordre);
              },
              error => {
                console.log(error);
              }
          )

    })
  }

  reset(){
    this.router.navigate(['../ordreTravailNonTraitees']);
  }

}
