import {Component, OnInit} from '@angular/core';
import {emplacementService} from '../services/emplacementService';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import Swal from 'sweetalert2';
import{ ToastrService} from 'ngx-toastr';

@Component({
    selector: 'app-add-emplacement',
    templateUrl: './add-emplacement.component.html',
    styleUrls: ['./add-emplacement.component.css']
})
export class AddEmplacementComponent implements OnInit {
    form: FormGroup
    errorMessage
    verifier: boolean
    verifier2: boolean
    constructor(private  emplacementService: emplacementService, private router: Router, private toast: ToastrService) {
        this.form = new FormGroup({
            reference: new FormControl(null, Validators.required),
            type: new FormControl(null, Validators.required),
            libelle_emplacement: new FormControl(null, Validators.required),
            description: new FormControl()
        })
    }

    ngOnInit() {
    }

    isValid(controlName) {
        return this.form.get(controlName).invalid && this.form.get(controlName).touched;
    }

    addEmplacement() {
        console.log(this.form.value);
        if (this.form.valid) {
            this.emplacementService.addEmplacement(this.form.value)
                .subscribe(
                    data => {
                        console.log(data);
                        this.router.navigate(['../emplacements']);
                        // this.toast.success('Emplacement ajouté avec succès','Emplacement ajouté avec succès');
                        Swal.fire({
                           position: 'center',
                            icon: 'success',
                            title: 'Emplacement ajouté avec succès',
                            showConfirmButton: false,
                            timer: 1500})
                    //  this.verifier = true
                    },
                    error => {
                        console.log(error.error['message']);
                        this.errorMessage = error.error['message'];
                       //this.verifier = false;
                       //      Swal.fire({
                       //          position: 'center',
                       //          icon: 'error',
                       //          title: 'Emplacement existe déja',
                       //      })
                    }
                );
            // if(this.verifier==true){
            //     this.router.navigate(['../emplacements']);
            //     Swal.fire({
            //         position: 'center',
            //         icon: 'success',
            //         title: 'Emplacement ajouté avec succès',
            //         showConfirmButton: false,
            //         timer: 1500
            //     })
            // }else{
            //     this.router.navigate(['../ajouterEmplacement']);
            //     Swal.fire({
            //         position: 'center',
            //         icon: 'error',
            //         title: 'Emplacement existe déja',
            //     })
            // }

        }
    }

    reset() {
        this.router.navigate(['../emplacements']);
    }
}
