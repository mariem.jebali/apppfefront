import { Component, OnInit } from '@angular/core';
import {ordreTravailService} from '../services/ordreTravailService';
import {Router, ActivatedRoute} from '@angular/router';
@Component({
  selector: 'app-affichage-intervenant',
  templateUrl: './affichage-intervenant.component.html',
  styleUrls: ['./affichage-intervenant.component.css']
})
export class AffichageIntervenantComponent implements OnInit {
id
  sub
  ordres
  arrayordres
  constructor(private   router: Router, private ActivatedRoute: ActivatedRoute,
              private  ordreService: ordreTravailService) { }

  ngOnInit() {
    this.getdetail()
  }
  getdetail() {
    this.sub = this.ActivatedRoute.params.subscribe(params => {
      this.id = params['id'];
      this.ordreService.intervOTDetail(this.id)
          .subscribe(data => {
                console.log(data)
                this.ordres = data;
                this.arrayordres=Array(this.ordres)
              },
              error => {
                console.log(error)
              }
          )
    })
  }
  backToMesOT(){
    this.router.navigate(['../mesOrdreTravail'])
  }

}
