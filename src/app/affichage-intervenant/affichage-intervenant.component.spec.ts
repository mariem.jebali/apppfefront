import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffichageIntervenantComponent } from './affichage-intervenant.component';

describe('AffichageIntervenantComponent', () => {
  let component: AffichageIntervenantComponent;
  let fixture: ComponentFixture<AffichageIntervenantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffichageIntervenantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffichageIntervenantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
