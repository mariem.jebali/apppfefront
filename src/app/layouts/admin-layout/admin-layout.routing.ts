import {Routes} from '@angular/router';
import {DashboardComponent} from '../../dashboard/dashboard.component';
import {ComptesComponent} from '../../comptes/comptes.component';
import {StockComponent} from '../../stock/stock.component';
import {FonctionsComponent} from '../../fonctions/fonctions.component';
import {EmplacementComponent} from '../../emplacement/emplacement.component';
import {PannesComponent} from '../../pannes/pannes.component';
import {ReclamationComponent} from '../../reclamation/reclamation.component';
import {AddcomptesComponent} from '../../addcomptes/addcomptes.component';
import {AddStockComponent} from '../../add-stock/add-stock.component';
import { AddFonctionComponent } from '../../add-fonction/add-fonction.component';
import { AddEmplacementComponent  } from '../../add-emplacement/add-emplacement.component';
import { AddPanneComponent } from '../../add-panne/add-panne.component';
import {EnvoyerReclamationComponent} from '../../envoyer-reclamation/envoyer-reclamation.component';
import {HomeComponent} from '../../home/home.component';
import { OrdreTravailIntervenantComponent} from '../../ordre-travail-Intervenant/ordre-travail-intervenant.component';
import { AffichagesGouvComponent } from '../../affichages-gouv/affichages-gouv.component';
import  { PlanningIntervComponent } from '../../planning-interv/planning-interv.component';
import { AffichageRespComponent } from '../../affichage-resp/affichage-resp.component';
import { ModifierEmplacmentComponent } from '../../modifier-emplacment/modifier-emplacment.component';
import { ModifierFonctionComponent } from '../../modifier-fonction/modifier-fonction.component';
import { ModifierCompteComponent } from '../../modifier-compte/modifier-compte.component';
import { ModifierPanneComponent } from '../../modifier-panne/modifier-panne.component';
import { ModifierStockComponent } from '../../modifier-stock/modifier-stock.component';
import { OrdreTravailResponsableComponent } from '../../ordre-travail-responsable/ordre-travail-responsable.component';
import { AffecterOtComponent } from '../../affecter-ot/affecter-ot.component';
import { ModifierOTComponent } from '../../modifier-ot/modifier-ot.component';
import { AffichageIntervenantComponent } from '../../affichage-intervenant/affichage-intervenant.component';
import { IntervenantListComponent } from '../../intervenant-list/intervenant-list.component';
import { TraiterOTComponent } from '../../traiter-ot/traiter-ot.component';
import { ModifierMotPasseComponent } from '../../modifier-mot-passe/modifier-mot-passe.component';
import { ImprimerOTComponent } from '../../imprimer-ot/imprimer-ot.component';
import { AffichageAdminComponent } from '../../affichage-admin/affichage-admin.component';
import {GuardVerificationGuard} from '../../guard-verification.guard';

export const AdminLayoutRoutes: Routes = [

    {path: 'Reclamations', component:   AffichageAdminComponent, canActivate:[GuardVerificationGuard]},
    {path: 'modifierMdp', component:   ModifierMotPasseComponent, canActivate:[GuardVerificationGuard]},
    {path: 'home', component: HomeComponent, canActivate:[GuardVerificationGuard]},
    {path: 'dashboard', component: DashboardComponent, canActivate:[GuardVerificationGuard]},
    {path: 'comptes', component: ComptesComponent, canActivate:[GuardVerificationGuard]},
    {path: 'ajouterCompte', component: AddcomptesComponent, canActivate:[GuardVerificationGuard]},
    {path: 'modifierCompte/:id', component: ModifierCompteComponent, canActivate:[GuardVerificationGuard]},
    {path: 'stock', component: StockComponent, canActivate:[GuardVerificationGuard]},
    {path: 'ajouterPiece', component: AddStockComponent, canActivate:[GuardVerificationGuard]},
    {path: 'modifierPiece/:id', component: ModifierStockComponent, canActivate:[GuardVerificationGuard]},
    {path: 'fonctions', component: FonctionsComponent, canActivate:[GuardVerificationGuard]},
    {path: 'ajouterFonction', component: AddFonctionComponent, canActivate:[GuardVerificationGuard]},
    {path: 'modifierFonction/:id', component: ModifierFonctionComponent, canActivate:[GuardVerificationGuard]},
    {path: 'emplacements', component: EmplacementComponent, canActivate:[GuardVerificationGuard]},
    {path: 'ajouterEmplacement', component: AddEmplacementComponent, canActivate:[GuardVerificationGuard]},
    {path: 'modifierEmplacement/:id', component: ModifierEmplacmentComponent, canActivate:[GuardVerificationGuard]},
    {path: 'pannes', component: PannesComponent, canActivate:[GuardVerificationGuard]},
    {path: 'ajouterPanne', component: AddPanneComponent, canActivate:[GuardVerificationGuard]},
    {path: 'modifierPanne/:id', component: ModifierPanneComponent, canActivate:[GuardVerificationGuard]},
    //path responsable
    {path: 'reclamationsRecu', component: ReclamationComponent, canActivate:[GuardVerificationGuard]},
    {path: 'reclamationsRecu/:id', component: AffichageRespComponent, canActivate:[GuardVerificationGuard]},
    {path: 'affecterReclamation/:id', component: AffecterOtComponent, canActivate:[GuardVerificationGuard]},
    {path: 'ordreTravailNonTraitees', component: OrdreTravailResponsableComponent, canActivate:[GuardVerificationGuard]},
    {path: 'ordreTravailNonTraiter/:id', component: AffichageRespComponent, canActivate:[GuardVerificationGuard]},
    {path: 'modifierOrdreTravail/:id', component: ModifierOTComponent, canActivate:[GuardVerificationGuard]},
    {path: 'ordreTravailTraitees', component: OrdreTravailResponsableComponent, canActivate:[GuardVerificationGuard]},
    {path: 'ordreTravailTraiter/:id', component: AffichageRespComponent, canActivate:[GuardVerificationGuard]},
    {path: 'ordreTravailCloturees', component: OrdreTravailResponsableComponent, canActivate:[GuardVerificationGuard]},
    {path: 'ordreTravailCloturer/:id', component: AffichageRespComponent, canActivate:[GuardVerificationGuard]},
    {path:'listIntervenant', component: IntervenantListComponent, canActivate:[GuardVerificationGuard] },
    {path:'planningIntervenant/:id', component: PlanningIntervComponent, canActivate:[GuardVerificationGuard] },
    {path: 'traiterOrdreTravail/:id', component: TraiterOTComponent, canActivate:[GuardVerificationGuard] },
    {path: 'imprimerOrdreTravail/:id', component: ImprimerOTComponent , canActivate:[GuardVerificationGuard] },
    //path intervenant
    {path: 'mesOrdreTravail', component:  OrdreTravailIntervenantComponent, canActivate:[GuardVerificationGuard]},
    {path: 'mesOrdreTravail/:id', component:AffichageIntervenantComponent , canActivate:[GuardVerificationGuard] },
    {path: 'traiterMonOrdre/:id', component:TraiterOTComponent , canActivate:[GuardVerificationGuard]},
    //gouvernante
    {path: 'reclamationsEnvoyees', component: ReclamationComponent, canActivate:[GuardVerificationGuard]},
    {path: 'envoyerReclamation', component: EnvoyerReclamationComponent, canActivate:[GuardVerificationGuard]},
    {path: 'reclamationsEnvoyees/:id', component: AffichagesGouvComponent, canActivate:[GuardVerificationGuard]},

];

