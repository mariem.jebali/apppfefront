import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AdminLayoutRoutes} from './admin-layout.routing';
import {DashboardComponent} from '../../dashboard/dashboard.component';
import {ComptesComponent} from '../../comptes/comptes.component';
import {StockComponent} from '../../stock/stock.component';
import {FonctionsComponent} from '../../fonctions/fonctions.component';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatRippleModule} from '@angular/material/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSelectModule} from '@angular/material/select';
import {EmplacementComponent} from '../../emplacement/emplacement.component';
import {PannesComponent} from '../../pannes/pannes.component';
import {ReclamationComponent} from '../../reclamation/reclamation.component';
import {AddcomptesComponent} from '../../addcomptes/addcomptes.component';
import {AddStockComponent} from '../../add-stock/add-stock.component';
import {AddEmplacementComponent} from '../../add-emplacement/add-emplacement.component';
import {AddPanneComponent} from '../../add-panne/add-panne.component';
import {EnvoyerReclamationComponent} from '../../envoyer-reclamation/envoyer-reclamation.component';
import {AddFonctionComponent} from '../../add-fonction/add-fonction.component';
import {HomeComponent } from '../../home/home.component';
import { OrdreTravailIntervenantComponent} from '../../ordre-travail-Intervenant/ordre-travail-intervenant.component';
import {NgSelectModule} from '@ng-select/ng-select';
 import { AffichagesGouvComponent } from '../../affichages-gouv/affichages-gouv.component';
import{ PlanningIntervComponent } from '../../planning-interv/planning-interv.component';
import { AffichageRespComponent } from '../../affichage-resp/affichage-resp.component';
import { ModifierEmplacmentComponent } from '../../modifier-emplacment/modifier-emplacment.component';
import { ModifierFonctionComponent } from '../../modifier-fonction/modifier-fonction.component';
import { ModifierCompteComponent } from '../../modifier-compte/modifier-compte.component';
import { ModifierPanneComponent } from '../../modifier-panne/modifier-panne.component';
import { ModifierStockComponent } from '../../modifier-stock/modifier-stock.component';
import { OrdreTravailResponsableComponent } from '../../ordre-travail-responsable/ordre-travail-responsable.component';
import { AffecterOtComponent } from '../../affecter-ot/affecter-ot.component';
import { ModifierOTComponent } from '../../modifier-ot/modifier-ot.component';
import { AffichageIntervenantComponent } from '../../affichage-intervenant/affichage-intervenant.component';
import { IntervenantListComponent } from '../../intervenant-list/intervenant-list.component';
import { TraiterOTComponent } from '../../traiter-ot/traiter-ot.component';
import { ModifierMotPasseComponent } from '../../modifier-mot-passe/modifier-mot-passe.component';
import { ImprimerOTComponent } from '../../imprimer-ot/imprimer-ot.component';
import { AffichageAdminComponent } from '../../affichage-admin/affichage-admin.component';
import {ChartsModule} from 'ng2-charts';
import {MatTabsModule} from '@angular/material/tabs';
import {MatIconModule} from '@angular/material/icon';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(AdminLayoutRoutes),
        FormsModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatRippleModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatTooltipModule,
        NgSelectModule,
        ChartsModule,
        ChartsModule,
        MatTabsModule,
        MatIconModule,
        MatTableModule,
        MatPaginatorModule,
    ],
    declarations: [
        DashboardComponent,
        ComptesComponent,
        StockComponent,
        FonctionsComponent,
        EmplacementComponent,
        PannesComponent,
        ReclamationComponent,
        AddcomptesComponent,
        AddStockComponent,
        AddEmplacementComponent,
        AddPanneComponent,
        EnvoyerReclamationComponent,
        AddFonctionComponent,
        HomeComponent,
        OrdreTravailIntervenantComponent,
        AffichagesGouvComponent,
        PlanningIntervComponent,
        AffichageRespComponent,
        ModifierEmplacmentComponent,
        ModifierFonctionComponent,
        ModifierCompteComponent,
        ModifierPanneComponent,
        ModifierStockComponent,
        OrdreTravailResponsableComponent,
        ModifierMotPasseComponent,
        AffecterOtComponent,
        ModifierOTComponent,
        AffichageIntervenantComponent,
        IntervenantListComponent,
        TraiterOTComponent,
        ImprimerOTComponent,
        AffichageAdminComponent,
    ]
})

export class AdminLayoutModule {
}
