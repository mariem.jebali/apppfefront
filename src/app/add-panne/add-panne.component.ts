import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import Swal from "sweetalert2";
import { panneService } from '../services/panneService';


@Component({
  selector: 'app-add-panne',
  templateUrl: './add-panne.component.html',
  styleUrls: ['./add-panne.component.css']
})
export class AddPanneComponent implements OnInit {
  form: FormGroup
    errorMessage

    verifier:boolean

  constructor(private  panneService:  panneService , private router: Router ){
    this.form = new FormGroup({
      libelle_panne: new FormControl(null, Validators.required),
    })
  }
  ngOnInit() {
  }

  isValid(controlName) {
    return this.form.get(controlName).invalid && this.form.get(controlName).touched;}

  addPanne(){
    console.log(this.form.value);
    if (this.form.valid) {
      this.panneService.addPanne(this.form.value)
          .subscribe(
              data => {
                console.log(data);
                  this.router.navigate(['../pannes']);
                  Swal.fire({
                      position: 'center',
                      icon: 'success',
                      title: 'Panne ajouté avec succès',
                      showConfirmButton: false,
                      timer: 1500
                  })
                  //this.verifier = true;
              },
              error => {
                console.log(error.error['message']);
              this.errorMessage = error.error['message']
                //  this.verifier =false;
              }
          );
      // if( this.verifier == true){
      //     this.router.navigate(['../pannes']);
      //     Swal.fire({
      //         position: 'center',
      //         icon: 'success',
      //         title: 'Panne ajouté avec succès',
      //         showConfirmButton: false,
      //         timer: 1500
      //     })
      // }else{
      //     Swal.fire({
      //         position: 'center',
      //         icon: 'error',
      //         title: 'Panne existe déja',
      //     })
      // }

    }}
    reset(){
        this.router.navigate(['../pannes']);
    }

}
