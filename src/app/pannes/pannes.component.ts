import {Component, OnInit} from '@angular/core';
import {panneService} from '../services/panneService';
import {Router} from '@angular/router';
import Swal from "sweetalert2";


@Component({
    selector: 'app-pannes',
    templateUrl: './pannes.component.html',
    styleUrls: ['./pannes.component.css']
})
export class PannesComponent implements OnInit {
    pannes

    constructor(private panneService: panneService,
                private   router: Router) {
    }

    ngOnInit() {
       this.getAllPannes();
    }
    getAllPannes(){
        this.panneService.getAllPannes()
            .subscribe(data => {
                    console.log(data)
                    this.pannes = data;
                },
                error => {
                    console.log(error)
                }
            )
    }
    deletePanne(panneId){
        Swal.fire({
            // title: 'Are you sure?',
            text: "Vous etes sur de vouloir supprimer ce panne ?",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Annuler',
            confirmButtonText: 'Supprimer'
        }).then((result) => {
            if (result.value) {
                console.log(panneId)
                this.panneService.deletePanne(panneId)
                    .subscribe(data => {
                            console.log(data)
                            this.ngOnInit()
                        },
                        error => { console.log(error)})
            }
        })
    }
    goToAdd(){
        this.router.navigate(['../ajouterPanne']);
    }

}
