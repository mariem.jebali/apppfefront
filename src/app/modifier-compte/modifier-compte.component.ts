import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import Swal from "sweetalert2";
import {utilisateurService} from '../services/utilisateurService';
import {fonctionsService} from '../services/fonctionsService';

@Component({
  selector: 'app-modifier-compte',
  templateUrl: './modifier-compte.component.html',
  styleUrls: ['./modifier-compte.component.css']
})
export class ModifierCompteComponent implements OnInit {
  form: FormGroup
  user
  sub
  id
  arrayUser
  fonctionId
    errorMessage
  constructor( private  fonctionsService: fonctionsService,
               private utilisateurService: utilisateurService, private router:Router, private ActivatedRoute:ActivatedRoute){
    this.form = new FormGroup({
      nom: new FormControl(null, Validators.required),
  prenom: new FormControl(null, Validators.required),
  telephone: new FormControl(null, Validators.required),
  email: new FormControl(null, Validators.required),
  mdp: new FormControl(null, Validators.required),
  fonctionId: new FormControl(null, Validators.required),
    })
  }

  ngOnInit() {
    this.getOneUser();
    this.getAllFonction();

  }
  getAllFonction() {
    this.fonctionsService.getAllFonction()
        .subscribe(
            data => {
              console.log(data)
              this.fonctionId = data
            },
            error => {
              console.log(error)
            })
  }
  isValid(controlName) {
    return this.form.get(controlName).invalid && this.form.get(controlName).touched;}

  getOneUser(){
    this.sub= this.ActivatedRoute.params.subscribe(params => {
      this.id= params['id'];
      console.log(this.id);
      this.utilisateurService.getOneUtilisateur(this.id)
          .subscribe( data =>{
                console.log(data);
                this.user= data
                  this.arrayUser=Array(this.user)
                  console.log(this.user)
                  this.form.get('fonctionId').setValue(this.user[0]['Fonction']['id'])
                  this.form.get('nom').setValue(this.user[0].nom)
                  this.form.get('prenom').setValue(this.user[0].prenom)
                  this.form.get('email').setValue(this.user[0].email)

                  this.form.get('telephone').setValue(this.user[0].telephone)
              console.log(this.user[0].Fonction.libelle_fonction)

              },
              error => {
                console.log(error);
              }
          )

    })
  }
  modifierUser(){

    console.log(this.form.value);
    if (this.form.valid) {
        this.sub= this.ActivatedRoute.params.subscribe(params => {
            this.id= params['id'];
      this.utilisateurService.modifierUtilisateurs(this.form.value,this.id)
          .subscribe(
              data => {
                console.log(data);
                this.router.navigate(['../comptes']);
                Swal.fire({
                  position: 'center',
                  icon: 'success',
                  title: 'Compte modifié avec succès',
                  showConfirmButton: false,
                  timer: 1500
                })
              },
              error => {
                console.log(error.error['message']);
                  this.errorMessage = error.error['message']

              }
          );
        })
    }
  }

  reset(){
    this.router.navigate(['../comptes']);
  }

}
