import { Component, OnInit } from '@angular/core';
import Swal from "sweetalert2";
import{ reclamationService } from '../services/reclamationService';
import {Router, ActivatedRoute} from '@angular/router';
import {ordreTravailService} from '../services/ordreTravailService';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {intervenantService} from '../services/intervenantService';
@Component({
  selector: 'app-traiter-ot',
  templateUrl: './affecter-ot.component.html',
  styleUrls: ['./affecter-ot.component.css']
})
export class AffecterOtComponent implements OnInit {

  formAffecter: FormGroup
  reclamation
  ordre
  sub
  id
  UtilisateurId
  arrayreclamation
  verifier:boolean
  constructor(private   router: Router,private intervenantService :intervenantService ,
              private ActivatedRoute:ActivatedRoute, private  ordreService: ordreTravailService,
              private  reclamationService :reclamationService ){
    this.formAffecter = new FormGroup({
      date_deb_interv: new FormControl(null, Validators.required),
      date_limit_interv: new FormControl(null, Validators.required),
      UtilisateurId: new FormControl(null, Validators.required),
    })
  }

  ngOnInit() {
  this.getReclamationOne();
  this.getAllIntervenant();
  }
  getReclamationOne(){
    this.sub = this.ActivatedRoute.params.subscribe(params => {
      this.id = params['id'];
      this.reclamationService.getRespOneReclamation(this.id)
          .subscribe(data => {
                console.log(data)
                this.reclamation = data;
                this.arrayreclamation= Array(this.reclamation)
              },
              error => {
                console.log(error)
              }
          )
    })

  }
  getAllIntervenant(){
    this.intervenantService.getAllIntervenant()
        .subscribe(data => {
              console.log(data)
              this.UtilisateurId = data;
            },
            error => {
              console.log(error)
            }
        )
  }
  isValid(controlName) {
    return this.formAffecter.get(controlName).invalid && this.formAffecter.get(controlName).touched;}

  Affecter(){
    console.log(this.formAffecter.value);
    if (this.formAffecter.valid) {
      this.sub= this.ActivatedRoute.params.subscribe(params => {
        this.id= params['id'];
        this.reclamationService.affecterReclamation(this.formAffecter.value,this.id)
            .subscribe(
                data => {
                  console.log(data);

                    this.router.navigate(['../reclamationsRecu']);
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Reclamation affecté avec succès',
                        showConfirmButton: false,
                        timer: 1500
                    })
                },
                error => {
                  console.log(error.error['message']);


                }
            );


      })

      }
  }

  backToRec() {
    this.router.navigate(['../reclamationsRecu'])
  }

}
