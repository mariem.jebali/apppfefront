import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffecterOtComponent } from './affecter-ot.component';

describe('TraiterOTComponent', () => {
  let component: AffecterOtComponent;
  let fixture: ComponentFixture<AffecterOtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffecterOtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffecterOtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
