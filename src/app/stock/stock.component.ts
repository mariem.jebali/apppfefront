import {Component, OnInit} from '@angular/core';
import {pieceService} from '../services/pieceService';
import Swal from 'sweetalert2'
import {Router, ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-table-list',
    templateUrl: './stock.component.html',
    styleUrls: ['./stock.component.css']
})
export class StockComponent implements OnInit {
    pieces

    constructor(private pieceService: pieceService,
                private   router: Router, private   ActivatedRoute: ActivatedRoute) {
    }

    ngOnInit() {
        this.getAllStock()
     }

    getAllStock() {
        this.pieceService.getAllPieces()
            .subscribe(data => {
                    console.log(data)
                    this.pieces = data;
                },
                error => {
                    console.log(error)
                }
            )
    }


    deletePiece(pieceId) {
        Swal.fire({
            // title: 'Are you sure?',
            text: "Vous etes sur de vouloir supprimer cette piece",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Annuler',
            confirmButtonText: 'Supprimer'
        }).then((result) => {
            if (result.value) {
                console.log(pieceId)
                // api
                this.pieceService.deletePiece(pieceId)
                    .subscribe(data => {
                            console.log(data)
                           this.ngOnInit()
                        },
                        error => { console.log(error)})
            }
        })
    }

    movetoadd() {
        this.router.navigate(['../ajouterPiece']);
    }

}
