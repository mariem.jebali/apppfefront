import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { emplacementService } from '../services/emplacementService';
import Swal from "sweetalert2";

@Component({
  selector: 'app-emplacement',
  templateUrl: './emplacement.component.html',
  styleUrls: ['./emplacement.component.css']
})
export class EmplacementComponent implements OnInit {
  emplacements
  constructor(private emplacementService: emplacementService ,
              private   router: Router ) { }

    ngOnInit() {
       this.getAllEmpalcement();
    }
 getAllEmpalcement(){
     this.emplacementService.getAllEmplacement()
         .subscribe( data =>{
                 console.log(data)
                 this.emplacements = data;
             },
             error => {
                 console.log(error)
             }
         )

 }
    deleteEmplacement(emplacemntId) {
        Swal.fire({
            // title: 'Are you sure?',
            text: "Vous etes sur de vouloir supprimer cet emplacement?",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Annuler',
            confirmButtonText: 'Supprimer'
        }).then((result) => {
            if (result.value) {
                console.log(emplacemntId)
                this.emplacementService.deleteEmplacement(emplacemntId)
                    .subscribe(data => {
                            console.log(data)
                            this.ngOnInit()
                        },
                        error => { console.log(error)})
            }
        })
    }

    goToAdd(){
        this.router.navigate(['../ajouterEmplacement']);
    }

}
