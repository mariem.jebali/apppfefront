import {Component, OnInit} from '@angular/core';
import {ordreTravailService} from '../services/ordreTravailService';
import {Router, ActivatedRoute} from '@angular/router';
import {reclamationService} from '../services/reclamationService';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
import Swal from 'sweetalert2';

@Component({
    selector: 'app-affichage-resp',
    templateUrl: './affichage-resp.component.html',
    styleUrls: ['./affichage-resp.component.css']
})
export class AffichageRespComponent implements OnInit {
    isNonTraiter: boolean;
    isTraiter: boolean;
    isCloturer: boolean;
    arrayreclamation
    reclamation
    isRecuDeteail: boolean
    sub
    id
    ordres

    href: string = ""
    intervenant

    constructor(private   router: Router, private ActivatedRoute: ActivatedRoute,
                private  ordreService: ordreTravailService, private reclamationService: reclamationService) {
    }

    ngOnInit() {
        this.Afficher();
    }

    Afficher() {

        if (this.router.url.startsWith('/ordreTravailTraiter/')) {
            this.getRespOtTraiter();
            this.isTraiter = true
        } else if (this.router.url.startsWith('/ordreTravailCloturer/')) {
            this.getRespOtCloturer();
            this.isCloturer = true
        } else if (this.router.url.startsWith('/ordreTravailNonTraiter/')) {
            this.getRespOtNonTraiter();
            this.isNonTraiter = true
        } else  {
            this.getOneReclamationDetail();
            this.isRecuDeteail = true
        }

    }

    getOneReclamationDetail() {
        this.sub = this.ActivatedRoute.params.subscribe(params => {
            this.id = params['id'];
            this.reclamationService.getRespOneReclamation(this.id)
                .subscribe(data => {
                        console.log(data)
                        this.reclamation = data;
                        this.arrayreclamation= Array(this.reclamation)
                    },
                    error => {
                        console.log(error)
                    }
                )
        })

    }

    getRespOtTraiter() {
        this.sub = this.ActivatedRoute.params.subscribe(params => {
            this.id = params['id'];
            this.ordreService.getOtTraiterDetail(this.id)
                .subscribe(data => {
                        console.log(data)
                        this.ordres = data;
                    },
                    error => {
                        console.log(error)
                    }
                )
        })
    }

    getRespOtCloturer() {
        this.sub = this.ActivatedRoute.params.subscribe(params => {
            this.id = params['id'];
            this.ordreService.getOtCloturerDetail(this.id)
                .subscribe(data => {
                        console.log(data)
                        this.ordres = data;
                    },
                    error => {
                        console.log(error)
                    }
                )
        })
    }

    getRespOtNonTraiter() {
        this.sub = this.ActivatedRoute.params.subscribe(params => {
            this.id = params['id'];
            this.ordreService.getOtNonTraiterDetail(this.id)
                .subscribe(data => {
                        console.log(data)
                        this.ordres = data;
                    },
                    error => {
                        console.log(error)
                    }
                )
        })
    }

    cloturerOT() {
        this.sub = this.ActivatedRoute.params.subscribe(params => {
            this.id = params['id'];
            this.ordreService.cloturerOT(this.id)
                .subscribe(data => {
                        console.log(data)
                        this.router.navigate(['../ordreTravailTraitees']);
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Ordre de travail cloturé avec succès',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    },
                    error => {
                        console.log(error)
                    }
                )
        })
    }

    backToTraiter() {
        this.router.navigate(['../ordreTravailTraitees'])
    }

    backToNonTraiter() {
        this.router.navigate(['../ordreTravailNonTraitees'])
    }

    backToCloturer() {
        this.router.navigate(['../ordreTravailCloturees'])
    }

    backToRec() {
        this.router.navigate(['../reclamationsRecu'])
    }
}
