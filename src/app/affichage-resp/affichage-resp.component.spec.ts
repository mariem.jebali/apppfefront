import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffichageRespComponent } from './affichage-resp.component';

describe('AffichageRespComponent', () => {
  let component: AffichageRespComponent;
  let fixture: ComponentFixture<AffichageRespComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffichageRespComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffichageRespComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
