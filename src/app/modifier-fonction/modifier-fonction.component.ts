import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import Swal from "sweetalert2";
import { fonctionsService } from '../services/fonctionsService';
@Component({
  selector: 'app-modifier-fonction',
  templateUrl: './modifier-fonction.component.html',
  styleUrls: ['./modifier-fonction.component.css']
})
export class ModifierFonctionComponent implements OnInit {
  form: FormGroup
fonction
  sub
  id
  Arrayfonction
    errorMessage

  constructor( private fonctionsService:fonctionsService, private router:Router, private ActivatedRoute:ActivatedRoute){
    this.form = new FormGroup({
      libelle_fonction: new FormControl(null, Validators.required),
    })
  }
  ngOnInit() {
    this.getOneFonction();
  }
  isValid(controlName) {
    return this.form.get(controlName).invalid && this.form.get(controlName).touched;}

  getOneFonction(){
    this.sub= this.ActivatedRoute.params.subscribe(params => {
      this.id= params['id'];
      console.log(this.id);
    this.fonctionsService.getOneFonction(this.id)
        .subscribe( data =>{
              console.log(data);
              this.fonction = data;
              this.Arrayfonction= Array(this.fonction)
                this.form.get('libelle_fonction').setValue(this.fonction.libelle_fonction)

            },
            error => {
              console.log(error);
            }
        )

  })
}
  modifierFonction(){
    this.sub= this.ActivatedRoute.params.subscribe(params => {
      this.id= params['id'];
      console.log(this.id);})
    console.log(this.form.value);
    if (this.form.valid) {
      this.fonctionsService.modifierFonction(this.form.value,this.id)
          .subscribe(
              data => {
                console.log(data);
                this.router.navigate(['../fonctions']);
                Swal.fire({
                  position: 'center',
                  icon: 'success',
                  title: 'Fonction modifié avec succès',
                  showConfirmButton: false,
                  timer: 1500
                })
              },
              error => {
                console.log(error.error['message']);
              this.errorMessage = error.error['message']

              }
          );

    }}

  reset(){
    this.router.navigate(['../fonctions']);
  }

}
