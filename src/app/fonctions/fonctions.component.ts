import { Component, OnInit } from '@angular/core';
import { fonctionsService } from '../services/fonctionsService';
import {Router} from '@angular/router';
import Swal from "sweetalert2";

@Component({
  selector: 'app-typography',
  templateUrl: './fonctions.component.html',
  styleUrls: ['./fonctions.component.css']
})
export class FonctionsComponent implements OnInit {
  fonctions
  constructor(private fonctionsService: fonctionsService ,
              private   router: Router ) { }

  ngOnInit() {
  this.getAllFonction();
  }
  getAllFonction(){
      this.fonctionsService.getAllFonction()
          .subscribe( data =>{
                  console.log(data)
                  this.fonctions = data;
              },
              error => {
                  console.log(error)
              }
          )

  }
  deleteFonction(fonctionId){
      Swal.fire({
          // title: 'Are you sure?',
          text: "Vous etes sur de vouloir supprimer cette fonction ?",
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'Annuler',
          confirmButtonText: 'Supprimer'
      }).then((result) => {
          if (result.value) {
              console.log(fonctionId)
              this.fonctionsService.deleteFonction(fonctionId)
                  .subscribe(data => {
                          console.log(data)
                          this.ngOnInit()
                      },
                      error => { console.log(error)})
          }
      })
  }

    goToAdd(){
        this.router.navigate(['../ajouterFonction']);
    }
}
