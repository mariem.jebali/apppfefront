import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class GuardVerificationGuard implements CanActivate {
    constructor(private   router: Router) {
    }

    canActivate() {
        console.log('start canactivate')
        if (localStorage.getItem('validtoken') === 'true') {
            return true;
        } else {
          console.log("else");
            this.router.navigate(['login']);
            return false;

        }


    }


}
