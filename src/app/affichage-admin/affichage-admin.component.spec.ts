import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffichageAdminComponent } from './affichage-admin.component';

describe('AffichageAdminComponent', () => {
  let component: AffichageAdminComponent;
  let fixture: ComponentFixture<AffichageAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffichageAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffichageAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
