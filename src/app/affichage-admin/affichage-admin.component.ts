import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
@Component({
  selector: 'app-affichage-admin',
  templateUrl: './affichage-admin.component.html',
  styleUrls: ['./affichage-admin.component.css']
})
export class AffichageAdminComponent implements OnInit {

  constructor(private   router: Router) { }

  ngOnInit(): void {
  }
  goToTraiter() {
    this.router.navigate(['../ordreTravailTraitees'])
  }

  goToNonTraiter() {
    this.router.navigate(['../ordreTravailNonTraitees'])
  }

  goToCloturer() {
    this.router.navigate(['../ordreTravailCloturees'])
  }
  goToRecu() {
    this.router.navigate(['../reclamationsRecu'])
  }
  goToPlanning() {
    this.router.navigate(['../listIntervenant'])
  }

}
