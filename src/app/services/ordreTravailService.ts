import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class ordreTravailService {
    baseUrl = 'http://localhost:3000/'
    constructor(private http: HttpClient) {}
    intervenantOrdreAll() {
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.get(this.baseUrl + 'api/utilisateurs/OrdreTravail/all/', {headers: headers })

    }
    getRespOtTraiter() {
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.get(this.baseUrl + 'api/OrdreTravail/traiter/all/', {headers: headers })
    }
    getRespOtNonTraiter() {
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.get(this.baseUrl + 'api/OrdreTravail/nontraiter/all/', {headers: headers })
    }
    getRespOtCloturer() {
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.get(this.baseUrl + 'api/OrdreTravail/cloturer/all/', {headers: headers })
    }
    getOtTraiterDetail(id:number){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.get(this.baseUrl + 'api/OrdreTravail/traiter/'+id +'/', {headers: headers })
    }
    getOtCloturerDetail(id:number){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.get(this.baseUrl + 'api/OrdreTravail/cloturer/'+id +'/', {headers: headers })
    }
    getOtNonTraiterDetail(id:number){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.get(this.baseUrl + 'api/OrdreTravail/nontraiter/'+id +'/', {headers: headers })
    }
    cloturerOT(id:number){
        let body=''
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.patch(this.baseUrl + 'api/OrdreTravail/'+id+ '/cloturer/',body ,{headers: headers })
    }
    modifierOT(body:any ,id:number){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.patch(this.baseUrl + 'api/OrdreTravail/'+id+ '/modifier/',body ,{headers: headers })
    }
    traiterOTResp(body:any ,id:number){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.patch(this.baseUrl + 'api/OrdreTravail/'+id+'/traiter/',body ,{headers: headers })
    }
    traiterOTInterv(body:any ,id:number){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.patch(this.baseUrl + 'api/intervenant/OrdreTravail/traiter/'+id+ '/',body ,{headers: headers })
    }
    intervOTDetail(id:number){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.get(this.baseUrl + 'api/utilisateurs/OrdreTravail/'+id+ '/',{headers: headers })
    }


}
