import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class intervenantService {
    baseUrl = 'http://localhost:3000/'

    constructor(private http: HttpClient) {}
    getAllIntervenant(){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.get(this.baseUrl + 'api/intervenant/all/', {headers: headers})
    }
    getPlanningInterv(id:number){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.get(this.baseUrl + 'api/intervenantPlanning/'+id+'/', {headers: headers})
    }

}
