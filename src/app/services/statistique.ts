import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class statistiqueService {
    baseUrl = 'http://localhost:3000/'
    constructor(private http: HttpClient) {}
    recEnvoyerJour(){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.get(this.baseUrl + 'api/reclamation/envoyer/all/', {headers: headers })
    }
    recTraiterJour(){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.get(this.baseUrl + 'api/reclamation/traiter/all/', {headers: headers })
    }
    recNonTraiterJour(){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.get(this.baseUrl + 'api/reclamation/nonTraiter/all/', {headers: headers })
    }
    gouvPanne(){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.get(this.baseUrl + 'api/reclamation/gouvPanne/all/', {headers: headers })
    }
    respectDate(){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.get(this.baseUrl + 'api/reclamation/respectdate/', {headers: headers })
    }
    nonRespectDate(){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.get(this.baseUrl + 'api/reclamation/nonrespectdate/', {headers: headers })
    }
    statpanne(){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.get(this.baseUrl + 'api/reclamation/panne/all/', {headers: headers })
    }
    getStockEpuise(){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.get(this.baseUrl + 'api/stockepuise/all/', {headers: headers })
    }
    statTypeEmplacement(){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.get(this.baseUrl + 'api/reclamations/typeEmplacement/all/', {headers: headers })
    }
}
