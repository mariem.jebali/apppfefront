import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class reclamationService {
    baseUrl = 'http://localhost:3000/'
    constructor(private http: HttpClient) {}
    getRespReclamation() {
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.get(this.baseUrl + 'api/reclamations/nonAffecter/all/', {headers: headers })
    }

    getRespOneReclamation(id:number){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.get(this.baseUrl + 'api/reclamations/nonAffecter/'+id +'/', {headers: headers })
    }
    getGouvReclamation(){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.get(this.baseUrl + 'api/utilisateurs/reclamations/all/', {headers: headers })
    }

    addReclamation(body: any) {
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.post(this.baseUrl + 'api/utilisateurs/reclamations/', body, { headers: headers})
    }
    getReclamationdetail(id:number){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.get(this.baseUrl + 'api/utilisateurs/reclamations/'+id +'/', {headers: headers })
    }
    affecterReclamation(body:any, id:number){
    let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
    return this.http.patch(this.baseUrl + 'api/reclamations/'+id +'/affecter/', body,{headers: headers })
}
}
