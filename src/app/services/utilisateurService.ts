import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class utilisateurService {
    baseUrl = 'http://localhost:3000/'

    constructor(private http: HttpClient) {
    }

    getAllUtilisateurs() {

        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.get(this.baseUrl + 'api/utilisateurs/all/', {headers: headers})
    }

    adduser(body: any) {
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.post(this.baseUrl + 'api/utilisateurs/', body, { headers: headers})
    }
    deleteUsers(id:number){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.delete(this.baseUrl + 'api/utilisateurs/'+id+'/', {headers: headers})
    }

    modifierUtilisateurs(body: any, id:number){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.put(this.baseUrl + 'api/utilisateurs/'+id, body, {observe: 'body', headers: headers})
    }
    getOneUtilisateur(id:number){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.get(this.baseUrl + 'api/utilisateurs/'+id+'/', {headers: headers})
    }
    modifierMotPasse(body:any){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.put(this.baseUrl + 'api/utilisateurs/', body,{headers: headers})
    }
}
