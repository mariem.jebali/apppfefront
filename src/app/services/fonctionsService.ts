import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class fonctionsService {
    baseUrl = 'http://localhost:3000/'

    constructor(private http: HttpClient) {
    }

    getAllFonction() {

        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.get(this.baseUrl + 'api/fonctions/all/', {headers: headers})
    }

    addFonction(body: any) {
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.post(this.baseUrl + 'api/fonctions/', body, { headers: headers})
    }
    deleteFonction(id:number){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.delete(this.baseUrl + 'api/fonctions/'+id+'/', {headers: headers})
    }
    modifierFonction(body: any, id:number){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.put(this.baseUrl + 'api/fonctions/'+id, body, {observe: 'body', headers: headers})
    }
    getOneFonction(id:number){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.get(this.baseUrl + 'api/fonctions/'+id+'/', {headers: headers})
    }
}
