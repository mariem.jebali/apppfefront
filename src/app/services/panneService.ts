import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class panneService {
    baseUrl = 'http://localhost:3000/'

    constructor(private http: HttpClient) {
    }

    getAllPannes() {
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.get(this.baseUrl + 'api/pannes/all/', {headers: headers})
    }

    addPanne(body: any) {
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.post(this.baseUrl + 'api/pannes/', body, { headers: headers})
    }
    deletePanne(id:number){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.delete(this.baseUrl + 'api/pannes/'+id+'/', {headers: headers})
    }
    modifierPanne(body: any, id:number){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.put(this.baseUrl + 'api/pannes/'+id, body, {observe: 'body', headers: headers})
    }
    getOnePanne(id:number){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.get(this.baseUrl + 'api/pannes/'+id+'/', {headers: headers})
    }
}
