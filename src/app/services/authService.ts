import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    baseUrl = 'http://localhost:3000/'
    constructor(private http: HttpClient) {}
    login(body: any) {
        return this.http.post(this.baseUrl + 'api/login/', body, {observe: 'body'})
    }
        getDetailMe(){
            let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
            return this.http.get(this.baseUrl + 'api/login/detailme/', {headers: headers})
        }
}
