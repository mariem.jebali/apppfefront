import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import 'rxjs-compat/operator/map';

@Injectable({
    providedIn: 'root'
})
export class pieceService {
    baseUrl = 'http://localhost:3000/'

    constructor(private http: HttpClient) {
    }

    getAllPieces() {
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.get(this.baseUrl + 'api/pieces/all/', {headers: headers})
    }
    getAllPieceUtilisee(){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.get(this.baseUrl + 'api/pieceutilisee/all/', {headers: headers})
    }

    addPiece(body: any) {
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.post(this.baseUrl + 'api/pieces/', body, { headers: headers})
    }
    deletePiece(id:number){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.delete(this.baseUrl + 'api/pieces/'+id+'/', {headers: headers})
    }
    modifierPiece(body: any,id:number){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.put(this.baseUrl + 'api/pieces/'+id, body, {observe: 'body', headers: headers})
    }
    getOnePiece(id:number){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.get(this.baseUrl + 'api/pieces/'+id+'/', {headers: headers})
    }
}
