import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class emplacementService {
    baseUrl = 'http://localhost:3000/'

    constructor(private http: HttpClient) {
    }

    getAllEmplacement() {

        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.get(this.baseUrl + 'api/emplacements/all/', {headers: headers})
    }

    addEmplacement(body: any) {
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.post(this.baseUrl + 'api/emplacements/', body, { headers: headers})
    }
    deleteEmplacement(id:number){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.delete(this.baseUrl + 'api/emplacements/'+id+'/', {headers: headers})
    }
    modifierEmplacement(body: any, id:number){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.put(this.baseUrl + 'api/emplacements/'+id, body, {observe: 'body', headers: headers})
    }
    getOneEmplacement(id:number){
        let headers = new HttpHeaders({'Authorization': localStorage.getItem('token')})
        return this.http.get(this.baseUrl + 'api/emplacements/'+id+'/', {headers: headers})
    }

}
