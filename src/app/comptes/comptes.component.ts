
import { Component, OnInit } from '@angular/core';
import {Router , ActivatedRoute} from '@angular/router';
import { utilisateurService } from '../services/utilisateurService';
import Swal from "sweetalert2";
@Component({
  selector: 'app-user-profile',
  templateUrl: './comptes.component.html',
  styleUrls: ['./comptes.component.css']
})
export class ComptesComponent implements OnInit {
  compte
    href: string = ""
  constructor(private utilisateurService : utilisateurService , private  router: Router,  ) { }

  ngOnInit() {
   this.getAllUsers();
  }

  getAllUsers(){
      this.utilisateurService.getAllUtilisateurs()
          .subscribe( data =>{
                  console.log(data)
                  this.compte = data;
              },
              error => {
                  console.log(error)
              }
          )
  }
    deleteUsers(userId) {
        Swal.fire({
            // title: 'Are you sure?',
            text: "Vous etes sur de vouloir supprimer ce compte?",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Annuler',
            confirmButtonText: 'Supprimer'
        }).then((result) => {
            if (result.value) {
                console.log(userId)
                this.utilisateurService.deleteUsers(userId)
                    .subscribe(data => {
                            console.log(data)
                            this.ngOnInit()
                        },
                        error => { console.log(error)})
            }
        })
    }

      movetoadd(){
    this.router.navigate(['../ajouterCompte']);
        }

}
