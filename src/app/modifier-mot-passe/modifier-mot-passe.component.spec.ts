import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifierMotPasseComponent } from './modifier-mot-passe.component';

describe('ModifierMotPasseComponent', () => {
  let component: ModifierMotPasseComponent;
  let fixture: ComponentFixture<ModifierMotPasseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifierMotPasseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifierMotPasseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
