import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import Swal from 'sweetalert2';
import {utilisateurService} from '../services/utilisateurService';

@Component({
    selector: 'app-modifier-mot-passe',
    templateUrl: './modifier-mot-passe.component.html',
    styleUrls: ['./modifier-mot-passe.component.css']
})
export class ModifierMotPasseComponent implements OnInit {
    errorMessage
    form: FormGroup

    isvalid: boolean

    constructor(private router: Router, private utilisateurService: utilisateurService) {
        this.form = new FormGroup({
            mdp: new FormControl(null, Validators.required),
            ancienmdp: new FormControl(null, Validators.required),
            confirmmdp: new FormControl(null, Validators.required),

        }, {validators: this.checkPasswords})

    }

    isValid(controlName) {
        return this.form.get(controlName).invalid && this.form.get(controlName).touched;
    }

    ngOnInit() {

    }

    checkPasswords(form: FormGroup) {
        let mdp = form.get('mdp').value;
        let confirmmdp = form.get('confirmmdp').value;

        return mdp === confirmmdp ? null : {notSame: true}
    }

    modifierMdp() {
        console.log(this.form.value);

        if (this.form.valid) {
            var valeur = {mdp: this.form.get('mdp').value, ancienmdp: this.form.get('ancienmdp').value}
            console.log(valeur.mdp);
            console.log(valeur.ancienmdp);
            this.utilisateurService.modifierMotPasse(valeur)
                .subscribe(
                    data => {
                        console.log(data);
                        this.router.navigate(['../home']);
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Mot de passe modifié avec succès',
                            showConfirmButton: false,
                            timer: 1500
                        })
                        //this.isvalid = true
                    },
                    error => {
                        console.log(error.error['message']);
                        this.errorMessage = error.error['message']
                        // Swal.fire({
                        //     position: 'center',
                        //     icon: 'error',
                        //     title: 'Ancien mot de passe incorrect',
                        // })
                        //this.isvalid = false
                    }
                );
            // if (this.isvalid == true) {
            //     this.router.navigate(['../home']);
            //     Swal.fire({
            //         position: 'center',
            //         icon: 'success',
            //         title: 'Mot de passe modifié avec succès',
            //         showConfirmButton: false,
            //         timer: 1500
            //     })
            // } else {
            //     Swal.fire({
            //         position: 'center',
            //         icon: 'error',
            //         title: 'Ancien mot de passe incorrect',
            //     })
            // }
        }
    }

    reset() {
        this.router.navigate(['../home']);
    }
}
