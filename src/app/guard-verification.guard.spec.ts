import { TestBed } from '@angular/core/testing';

import { GuardVerificationGuard } from './guard-verification.guard';

describe('GuardVerificationGuard', () => {
  let guard: GuardVerificationGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(GuardVerificationGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
