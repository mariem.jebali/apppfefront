import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { reclamationService } from '../services/reclamationService';

import { AuthService } from '../services/authService';

@Component({
  selector: 'app-reclamation',
  templateUrl: './reclamation.component.html',
  styleUrls: ['./reclamation.component.css']
})
export class ReclamationComponent implements OnInit {
  reclamations
detailUser
    href: string = ""
    isRecu:boolean;
  isgouv:boolean;

  constructor(private reclamationService : reclamationService,
              private   router: Router , private AuthService:AuthService ) {}

  ngOnInit() {
this.getReclamations()
  }
    getReclamations(){
        this.AuthService.getDetailMe()
            .subscribe( data =>{
                    console.log(data)
                    this.detailUser = data;
                    this.href = this.router.url;
                 if (((this.detailUser.fonctionId == 3)||(this.detailUser.fonctionId == 1))&&(this.href == '/reclamationsRecu')){
                    this.getRespReclamation();
                    this. isRecu=true
                } else if (this.detailUser.fonctionId == 2){
                        this.getGouvReclamation();
                        this.isgouv=true;
                    }
                },
                error => {
                    console.log(error)
                }
            )
    }


    getGouvReclamation(){
        this.reclamationService.getGouvReclamation()
            .subscribe( data =>{
                    console.log(data)
                    this.reclamations= data;
                },
                error => {
                    console.log(error)
                }
            )
    }

    getRespReclamation(){
        this.reclamationService.getRespReclamation()
            .subscribe( data =>{
                    console.log(data)
                    this.reclamations= data;
                },
                error => {
                    console.log(error)
                }
            )
    }


}
