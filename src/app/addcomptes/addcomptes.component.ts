import {Component, OnInit} from '@angular/core';
import {utilisateurService} from '../services/utilisateurService';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {fonctionsService} from '../services/fonctionsService';
import Swal from 'sweetalert2';

@Component({
    selector: 'app-addcomptes',
    templateUrl: './addcomptes.component.html',
    styleUrls: ['./addcomptes.component.css']
})
export class AddcomptesComponent implements OnInit {
    errorMessage
    fonctionId
    form: FormGroup
verifier:boolean
    constructor(private  fonctionsService: fonctionsService,
                private utilisateurService: utilisateurService, private router: Router) {
        this.form = new FormGroup({
            nom: new FormControl(null, Validators.required),
            prenom: new FormControl(null, Validators.required),
            telephone: new FormControl(null, Validators.required),
            email: new FormControl(null, Validators.required),
            mdp: new FormControl(null, Validators.required),
            fonctionId: new FormControl(null, Validators.required),
        })
    }

    ngOnInit() {
        this.getAllFonction();

    }

    getAllFonction() {
        this.fonctionsService.getAllFonction()
            .subscribe(
                data => {
                    console.log(data)
                    this.fonctionId = data
                },
                error => {
                    console.log(error)
                })
    }

    isValid(controlName) {
        return this.form.get(controlName).invalid && this.form.get(controlName).touched;
    }

    adduser() {
        console.log(this.form.value);
        if (this.form.valid) {
         this.utilisateurService.adduser(this.form.value)
                .subscribe(
                    data => {
                        console.log(data);
                        // this.router.navigate(['../comptes']);
                        // Swal.fire({
                        //     position: 'center',
                        //     icon: 'success',
                        //     title: 'Compte ajouté avec succès',
                        //     showConfirmButton: false,
                        //     timer: 1500
                        // })
                  this.verifier=true
                    },
                    error => {
                        console.log(error.error['message']);
                        this.errorMessage = error.error['message']
                       this.verifier=false



                    }
                )
            if( this.verifier==true){
                this.router.navigate(['../comptes']);
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Compte ajouté avec succès',
                    showConfirmButton: false,
                    timer: 1500
                })
            }else{
                // this.router.navigate(['../ajouterCompte']);
                Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: 'Email existe déja',
                })
            };

        }
    }
    reset(){
        this.router.navigate(['../comptes']);
    }

}
