import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import Swal from "sweetalert2";
import { pieceService } from '../services/pieceService';
@Component({
  selector: 'app-modifier-stock',
  templateUrl: './modifier-stock.component.html',
  styleUrls: ['./modifier-stock.component.css']
})
export class ModifierStockComponent implements OnInit {
  form: FormGroup
  piece
  sub
  id
  Arraypiece
    errorMessage
  constructor( private pieceService:pieceService, private router:Router, private ActivatedRoute:ActivatedRoute ){
    this.form = new FormGroup({
      libelle_piece: new FormControl(null, Validators.required),
      quantite: new FormControl(null, Validators.required),
    })
  }
  ngOnInit() {
    this.getOnePiece();
  }
  isValid(controlName) {
    return this.form.get(controlName).invalid && this.form.get(controlName).touched;}

  getOnePiece(){
    this.sub= this.ActivatedRoute.params.subscribe(params => {
      this.id= params['id'];
      console.log(this.id);
      this.pieceService.getOnePiece(this.id)
          .subscribe( data =>{
                console.log(data);
                this.piece = data;
                this.Arraypiece= Array(this.piece)
                  this.form.get('libelle_piece').setValue(this.piece.libelle_piece)
                  this.form.get('quantite').setValue(this.piece.quantite)
              },
              error => {
                console.log(error);
              }
          )

    })
  }
  modifierPiece(){
    console.log(this.form.value);
    if (this.form.valid) {
        this.sub= this.ActivatedRoute.params.subscribe(params => {
            this.id = params['id'];
            this.pieceService.modifierPiece(this.form.value, this.id)
                .subscribe(
                    data => {
                        console.log(data);
                        this.router.navigate(['../stock']);
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Piece modifié avec succès',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    },
                    error => {
                        console.log(error.error['message']);
                        this.errorMessage = error.error['message']

                    }
                );
        })

    }}

  reset(){
    this.router.navigate(['../pieces']);
  }

}
