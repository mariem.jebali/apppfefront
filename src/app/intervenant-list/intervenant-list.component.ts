import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {intervenantService} from '../services/intervenantService';
@Component({
  selector: 'app-intervenant-list',
  templateUrl: './intervenant-list.component.html',
  styleUrls: ['./intervenant-list.component.css']
})
export class IntervenantListComponent implements OnInit {
intervenant
  constructor( private intervenantService :intervenantService ,private   router: Router,) { }

  ngOnInit() {
    this.getAllIntervenant();
  }


  getAllIntervenant(){
    this.intervenantService.getAllIntervenant()
        .subscribe(data => {
              console.log(data)
              this.intervenant = data;
            },
            error => {
              console.log(error)
            }
        )
  }
}
