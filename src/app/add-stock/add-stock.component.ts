import { Component, OnInit } from '@angular/core';
import { pieceService } from '../services/pieceService';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import Swal from "sweetalert2";

@Component({
  selector: 'app-add-stock',
  templateUrl: './add-stock.component.html',
  styleUrls: ['./add-stock.component.css']
})
export class AddStockComponent implements OnInit {
form: FormGroup
    errorMessage

    verifier:boolean

  constructor(private  pieceService:  pieceService , private router: Router ){
    this.form = new FormGroup({
      libelle_piece: new FormControl(null, Validators.required),
      quantite: new FormControl(null, Validators.required),

    })
  }
  ngOnInit() {
  }

  isValid(controlName) {
    return this.form.get(controlName).invalid && this.form.get(controlName).touched;}

  addPiece(){
    console.log(this.form.value);
    if (this.form.valid) {

      this.pieceService.addPiece(this.form.value)
          .subscribe(
              data => {
                console.log(data);
                  this.router.navigate(['../stock']);
                  Swal.fire({
                      position: 'center',
                      icon: 'success',
                      title: 'Pièce ajouté avec succès',
                      showConfirmButton: false,
                      timer: 1500
                  })
                  //this.verifier=true;
              },
              error => {
                console.log(error.error['message']);
             this.errorMessage = error.error['message']
                  //this.verifier=false;
              }
          );
      // if(this.verifier ==true){
      //     this.router.navigate(['../stock']);
      //     Swal.fire({
      //         position: 'center',
      //         icon: 'success',
      //         title: 'Pièce ajouté avec succès',
      //         showConfirmButton: false,
      //         timer: 1500
      //     })
      // }else{
      //
      //     Swal.fire({
      //         position: 'center',
      //         icon: 'error',
      //         title: 'Pièce existe déja',
      //     })
      // }


    }}
    reset(){
        this.router.navigate(['../stock']);
    }

}
