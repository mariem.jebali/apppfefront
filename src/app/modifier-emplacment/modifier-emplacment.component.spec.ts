import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifierEmplacmentComponent } from './modifier-emplacment.component';

describe('ModifierEmplacmentComponent', () => {
  let component: ModifierEmplacmentComponent;
  let fixture: ComponentFixture<ModifierEmplacmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifierEmplacmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifierEmplacmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
