import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import Swal from "sweetalert2";
import  { emplacementService } from '../services/emplacementService';
@Component({
  selector: 'app-modifier-emplacment',
  templateUrl: './modifier-emplacment.component.html',
  styleUrls: ['./modifier-emplacment.component.css']
})
export class ModifierEmplacmentComponent implements OnInit {
  form: FormGroup
  emplacement
  sub
  id
  Arrayemplacement
    errorMessage
  constructor( private emplacementService:emplacementService, private router:Router, private ActivatedRoute:ActivatedRoute){
    this.form = new FormGroup({
      reference: new FormControl(null, Validators.required),
      type: new FormControl(null, Validators.required),
      libelle_emplacement: new FormControl(null, Validators.required),
      description: new FormControl()
    })
  }
  ngOnInit() {
    this.getOneEmplacement();
  }
  isValid(controlName) {
    return this.form.get(controlName).invalid && this.form.get(controlName).touched;}

  getOneEmplacement(){
    this.sub= this.ActivatedRoute.params.subscribe(params => {
      this.id= params['id'];
      console.log(this.id);
      this.emplacementService.getOneEmplacement(this.id)
          .subscribe( data =>{
                console.log(data);
                this.emplacement = data;
                this.Arrayemplacement= Array(this.emplacement)
                  this.form.get('reference').setValue(this.emplacement.reference)
                  this.form.get('type').setValue(this.emplacement.type)
                  this.form.get('libelle_emplacement').setValue(this.emplacement.libelle_emplacement)
                  this.form.get('description').setValue(this.emplacement.description)
              },
              error => {
                console.log(error);
              }
          )

    })
  }
  modifierEmplacement(){
          console.log(this.form.value);
          if (this.form.valid) {
              this.sub= this.ActivatedRoute.params.subscribe(params => {
                  this.id = params['id'];
              this.emplacementService.modifierEmplacement(this.form.value, this.id)
                  .subscribe(
                      data => {
                          console.log(data);
                          this.router.navigate(['../emplacements']);
                          Swal.fire({
                              position: 'center',
                              icon: 'success',
                              title: 'Emplacement modifié avec succès',
                              showConfirmButton: false,
                              timer: 1500
                          })
                      },
                      error => {
                          console.log(error.error['message']);
                          this.errorMessage = error.error['message']

                      }
                  );

          })
          }

      }

  reset(){
    this.router.navigate(['../emplacements']);
  }
}
