import { Component, OnInit } from '@angular/core';
import {Router , ActivatedRoute} from '@angular/router';
import { intervenantService } from '../services/intervenantService';

@Component({
  selector: 'app-planning-interv',
  templateUrl: './planning-interv.component.html',
  styleUrls: ['./planning-interv.component.css']
})
export class PlanningIntervComponent implements OnInit {
  planning
    id
    sub
    arrayplanning
  constructor(private intervenantService : intervenantService , private   router: Router, private ActivatedRoute:ActivatedRoute) { }
  ngOnInit() {
    this.getPlanningInterv();
  }
    getPlanningInterv(){
        this.sub= this.ActivatedRoute.params.subscribe(params => {
            this.id= params['id'];
            console.log(this.id);
            this.intervenantService.getPlanningInterv(this.id)
                .subscribe( data =>{
                        console.log(data);
                        this.planning = data;
                    },
                    error => {
                        console.log(error)
                    }
                )
        });
    }

    bacKtOList(){

        this.router.navigate(['../listIntervenant'])
    }
}
