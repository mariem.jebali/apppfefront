import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanningIntervComponent } from './planning-interv.component';

describe('PlanningIntervComponent', () => {
  let component: PlanningIntervComponent;
  let fixture: ComponentFixture<PlanningIntervComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanningIntervComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanningIntervComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
