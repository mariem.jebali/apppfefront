import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import Swal from "sweetalert2";
import { fonctionsService } from '../services/fonctionsService';

@Component({
  selector: 'app-add-fonction',
  templateUrl: './add-fonction.component.html',
  styleUrls: ['./add-fonction.component.css']
})
export class AddFonctionComponent implements OnInit {
  form: FormGroup
    errorMessage

    verifier:boolean

  constructor( private fonctionsService:fonctionsService, private router:Router){
    this.form = new FormGroup({
      libelle_fonction: new FormControl(null, Validators.required),
    })
  }
  ngOnInit() {
  }

  isValid(controlName) {
    return this.form.get(controlName).invalid && this.form.get(controlName).touched;}

  addFonction(){
    console.log(this.form.value);
    if (this.form.valid) {

      this.fonctionsService.addFonction(this.form.value)
          .subscribe(
              data => {
                console.log(data);
                 // this.verifier=true
                  this.router.navigate(['../fonctions']);
                  Swal.fire({
                      position: 'center',
                      icon: 'success',
                      title: 'Fonction ajouté avec succès',
                      showConfirmButton: false,
                      timer: 1500
                  })

              },
              error => {
                  console.log(error.error['message']);
                  this.errorMessage = error.error['message']
            //   this.verifier=false


              }
          );
        // if( this.verifier == true){
        //     this.router.navigate(['../fonctions']);
        //     Swal.fire({
        //         position: 'center',
        //         icon: 'success',
        //         title: 'Fonction ajouté avec succès',
        //         showConfirmButton: false,
        //         timer: 1500
        //     })
        // }else{
        //
        //     Swal.fire({
        //         position: 'center',
        //         icon: 'error',
        //         title: 'Foncion existe déja',
        //     })
        // }


    }}
    reset(){
        this.router.navigate(['../fonctions']);
    }

}
