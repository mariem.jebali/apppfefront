import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdreTravailIntervenantComponent } from './ordre-travail-intervenant.component';

describe('OrdreTravailComponent', () => {
  let component: OrdreTravailIntervenantComponent;
  let fixture: ComponentFixture<OrdreTravailIntervenantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdreTravailIntervenantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdreTravailIntervenantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
