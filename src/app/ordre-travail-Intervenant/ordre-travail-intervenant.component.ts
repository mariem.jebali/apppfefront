import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import{ ordreTravailService } from '../services/ordreTravailService';
import { AuthService } from '../services/authService';
@Component({
  selector: 'app-ordre-travail',
  templateUrl: './ordre-travail-intervenant.component.html',
  styleUrls: ['./ordre-travail-intervenant.component.css']
})
export class OrdreTravailIntervenantComponent implements OnInit {
 ordres
    detailUser
    href: string = ""


  constructor(private ordreService : ordreTravailService) { }

  ngOnInit() {
      this.getIntervenantAllOt();

  }

    getIntervenantAllOt()
    {this.ordreService.intervenantOrdreAll()
        .subscribe( data =>{
                console.log(data)
                this.ordres= data;
            },
            error => {
                console.log(error)
            }
        )}


}
