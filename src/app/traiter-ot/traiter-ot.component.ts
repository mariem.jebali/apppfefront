import { Component, OnInit } from '@angular/core';
import Swal from "sweetalert2";
import {Router, ActivatedRoute} from '@angular/router';
import {ordreTravailService} from '../services/ordreTravailService';
import { pieceService } from '../services/pieceService';
import {FormControl, FormGroup, Validators, FormBuilder, FormArray} from '@angular/forms';
import { AuthService } from '../services/authService';
@Component({
  selector: 'app-traiter-ot',
  templateUrl: './traiter-ot.component.html',
  styleUrls: ['./traiter-ot.component.css']
})
export class TraiterOTComponent implements OnInit {
formTraiter: FormGroup
piece
    detailUser
    PieceStockId: FormArray
    ordres
  arrayordres
  arraypieces
  id
  sub
  isInterv :boolean
  isResp:boolean
  constructor(private router: Router,private ActivatedRoute:ActivatedRoute, private  ordreService: ordreTravailService,
      private  pieceService:pieceService, private formBuilder:FormBuilder,private AuthService:AuthService ){

      this.formTraiter =this.formBuilder.group({
          date_fin_inter : ['',Validators.required] ,
          description_interv : ['',Validators.required],
          PieceStockId: this.formBuilder.array([]) ,
          quantite: this.formBuilder.array([])
      })
  }

  ngOnInit(){
  this.getAllpiece();
  this.afficher();
  }

  getPiece() : FormArray {
    return this.formTraiter.get('PieceStockId') as FormArray ;
  }
    getquantite() : FormArray {
        return this.formTraiter.get('quantite') as FormArray ;
    }
    addquantite(event){
        const control2 = this.formBuilder.control(event.target.value,Validators.required) ;
        this.getquantite().push(control2) ;
    }
  addPiece(){
    const control = this.formBuilder.control(null,Validators.required) ;
    this.getPiece().push(control) ;

  }

  isValid(controlName) {
    return this.formTraiter.get(controlName).invalid && this.formTraiter.get(controlName).touched;}


  afficher(){
      this.AuthService.getDetailMe()
          .subscribe( data =>{
                  console.log(data)
                  this.detailUser = data;
                  if ((this.router.url.startsWith('/traiterOrdreTravail/'))&& ((this.detailUser.fonctionId == 3)||(this.detailUser.fonctionId == 1))) {
                        console.log( this.detailUser)
                      this.getOTResp();
                      this.isResp = true
                  } else {
                      console.log( this.detailUser)
                      this.getOTInterv();
                      this.isInterv = true
                  }
              },
              error => {
                  console.log(error)
              }
          )
  }
    intervTraiter(){
    console.log(this.formTraiter.value)
    this.sub = this.ActivatedRoute.params.subscribe(params => {
      this.id = params['id'];
        console.log(this.id)
        this.ordreService.traiterOTInterv(this.formTraiter.value,this.id)
          .subscribe(data => {
                console.log(data)
                  this.router.navigate(['../mesOrdreTravail']);

                  Swal.fire({
                  position: 'center',
                  icon: 'success',
                  title: 'Ordre de travail traité avec succès',
                  showConfirmButton: false,
                  timer: 1500
                })
              },
              error => {
                console.log(error)
              }
          )
    })
  }
    respTraiter (){
        console.log(this.formTraiter.value)
        this.sub = this.ActivatedRoute.params.subscribe(params => {
            this.id = params['id'];
            console.log(this.id)
            this.ordreService.traiterOTResp(this.formTraiter.value,this.id)
                .subscribe(data => {
                        console.log(data)
                        this.router.navigate(['../ordreTravailTraitees']);

                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Ordre de travail traité avec succès',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    },
                    error => {
                        console.log(error)
                    }
                )
        })
    }

  getAllpiece(){
    this.pieceService.getAllPieceUtilisee()
        .subscribe(data => {
              console.log(data)
              this.piece = data;
              this.arraypieces= Array(this.piece)

            },
            error => {
              console.log(error)
            }
        )

  }

  getOTInterv(){
      this.sub = this.ActivatedRoute.params.subscribe(params => {
          this.id = params['id'];
          this.ordreService.intervOTDetail(this.id)
              .subscribe(data => {
                      console.log(data)
                      this.ordres = data;
                      this.arrayordres=Array(this.ordres)
                  },
                  error => {
                      console.log(error)
                  }
              )
      })
  }

  getOTResp(){
    this.sub = this.ActivatedRoute.params.subscribe(params => {
      this.id = params['id'];
      this.ordreService.getOtNonTraiterDetail(this.id)
          .subscribe(data => {
                console.log(data)
                this.ordres = data;
              },
              error => {
                console.log(error)
              }
          )
    })

  }

  backToMesOT(){
    this.router.navigate(['../mesOrdreTravail'])
  }
  backToOT(){
    this.router.navigate(['../ordreTravailNonTraitees'])
  }
}
