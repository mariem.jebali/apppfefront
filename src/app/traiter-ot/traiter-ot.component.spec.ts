import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TraiterOTComponent } from './traiter-ot.component';

describe('TraiterOTComponent', () => {
  let component: TraiterOTComponent;
  let fixture: ComponentFixture<TraiterOTComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TraiterOTComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TraiterOTComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
