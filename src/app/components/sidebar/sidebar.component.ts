import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/authService';
import { Router} from '@angular/router';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
icon : string;
    class: string;
}

// admin routes
export const ROUTES1: RouteInfo[] = [
    { path: '/dashboard', title: 'Tableau de bord', icon: 'dashboard', class: '' },
    { path: '/comptes', title: 'Gestion des comptes', icon: 'person', class: '' },
    { path: '/stock', title: 'Gestion de stock',  icon: 'home', class: '' },
    { path: '/fonctions', title: 'Gestion des fonctions', icon: 'work',  class: '' },
    { path: '/emplacements', title: 'Gestion des emplacements', icon: 'home_work',  class: '' },
    { path: '/pannes', title: 'Gestion des pannes', icon: 'report_problem',  class: '' },
    {path: '/Reclamations', title: 'Suivi des reclamations', icon: 'assignment',  class: ''  },

];

//gouvernante routes
export const ROUTES2: RouteInfo[] = [
    { path: '/reclamationsEnvoyees', title: 'Reclamations envoyées', icon: 'assignment',  class: '' },
    { path: '/envoyerReclamation', title: 'Envoyer une reclamation', icon: 'assignment',  class: '' },

];
//responsable routes
export const ROUTES3: RouteInfo[] = [
    { path: '/dashboard', title: 'Tableau de bord', icon: 'dashboard', class: '' },
    { path: '/emplacements', title: 'Gestion des emplacements', icon: 'home_work',  class: '' },
    { path: '/pannes', title: 'Gestion des pannes', icon: 'report_problem',  class: '' },
    { path: '/reclamationsRecu', title: 'reclamations reçus', icon: 'assignment',  class: '' },
    {path: '/ordreTravailNonTraitees', title: 'OT non traitées', icon: 'assignment',  class: ''  },
    {path: '/ordreTravailTraitees',  title: 'OT traitées', icon: 'assignment',  class: '' },
    {path: '/ordreTravailCloturees', title: 'OT colturées', icon: 'assignment',  class: ''  },
    {path: '/listIntervenant', title: 'Liste des intervenants', icon: 'calendar_today',  class: ''  },

];

//intervenant routes
export const ROUTES4: RouteInfo[] = [
    { path: '/mesOrdreTravail', title: 'Mes ordres de travail', icon: 'work',  class: '' },
   // { path: '/modifierMdp', title: 'Modifer mot de passe', icon: 'report_problem',  class: '' },
];


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];
    detailUser;

  constructor( private AuthService:AuthService, private router:Router) { }

  ngOnInit() {

    this.getUserDetail()
  }
  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  };
  getUserDetail(){
      this.AuthService.getDetailMe()
          .subscribe( data =>{
                  console.log(data)
                  this.detailUser = data;
                  if(this.detailUser.fonctionId == 1){
                      this.menuItems = ROUTES1.filter(menuItem => menuItem);
                   }
                 else if(this.detailUser.fonctionId == 2)
                  {this.menuItems = ROUTES2.filter(menuItem => menuItem);
                   }
                  else if(this.detailUser.fonctionId == 3)
                  {this.menuItems = ROUTES3.filter(menuItem => menuItem);
                  }
                 else { this.menuItems = ROUTES4.filter(menuItem => menuItem);}
              },
              error => {
                  console.log(error)
              }
          )
  }

    goToHome(){
        this.router.navigate(['../home']);
    }

}
