import { Component, OnInit, ElementRef } from '@angular/core';
import { ROUTES1,ROUTES2,ROUTES3,ROUTES4 } from '../sidebar/sidebar.component';
import {Location} from '@angular/common';
import { Router } from '@angular/router';
import { AuthService } from '../../services/authService';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
    private listTitles: any[];
    location: Location;

    detailUser
    constructor(location: Location ,private AuthService:AuthService , private router: Router) {this.location = location;}
    ngOnInit() {
       this.getUserDetail()
    }
    getTitle() {
      let titlee = this.location.prepareExternalUrl(this.location.path());
      if (titlee.charAt(0) === '#') {
          titlee = titlee.slice( 1 );
      }
      for ( let item = 0; item < this.listTitles.length; item++){
          if (this.listTitles[item].path === titlee){
              return this.listTitles[item].title;
          }
      }
    }
    getUserDetail(){
        this.AuthService.getDetailMe()
            .subscribe( data =>{
                    console.log(data)
                    this.detailUser = data;
                    if(this.detailUser.fonctionId == 1){
                        this.listTitles = ROUTES1.filter(listTitle => listTitle);}
                    else if(this.detailUser.fonctionId == 2)
                    { this.listTitles = ROUTES2.filter(listTitle => listTitle);
                    } else if(this.detailUser.fonctionId == 3)
                    { this.listTitles = ROUTES3.filter(listTitle => listTitle);
                    }
                    else{this.listTitles = ROUTES4.filter(listTitle => listTitle);}
                },
                error => {
                    console.log(error)
                }
            )
    }

    logout() {
      localStorage.removeItem('token');
      localStorage.removeItem('validtoken');
        this.router.navigate(['../login']);
    }

    goToModifier(){
        this.router.navigate(['../modifierMdp']);
    }

}
